﻿using Microsoft.AspNetCore.Authentication;
using Auth0.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using DeckbuilderWeb.ViewModels.Account;
using System.Security.Claims;
using DeckbuilderWeb.Services;
using DeckbuilderWeb.Models.Account;

namespace DeckbuilderWeb.Controllers.Account;

/// <summary>
/// Account controller
/// </summary>
public class AccountController : Controller
{
    private readonly IDeckbuilderService _service;

    /// <summary>
    /// Default constructor
    /// </summary> 
    public AccountController()
    {
        _service = new DeckbuilderService();
    }

    /// <summary>
    /// User login
    /// </summary>
    /// <returns></returns>
    public async Task Login()
    {
        var authenticationProperties = new LoginAuthenticationPropertiesBuilder()
            .WithRedirectUri(Url.Action("Profile"))
            .Build();
        await HttpContext.ChallengeAsync(Auth0Constants.AuthenticationScheme, authenticationProperties);
    }

    /// <summary>
    /// User logout
    /// </summary>
    /// <returns></returns>
    [Authorize]
    public async Task Logout()
    {
        var authenticationProperties = new LogoutAuthenticationPropertiesBuilder()
            .WithRedirectUri(Url.Action("Index", "Home"))
            .Build();

        await HttpContext.SignOutAsync(Auth0Constants.AuthenticationScheme, authenticationProperties);
        await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
    }

    /// <summary>
    /// User profile
    /// </summary>
    /// <returns></returns>
    [Authorize]
    public async Task<IActionResult> Profile()
    {
        var id = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault();
        User model;
        if (!await _service.IsUserRegisteredAsync(id))
        {
            model = new()
            {
                Id = id,
                Email = User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).FirstOrDefault()
            };
            if (!await _service.RegisterUserAsync(model))
            {
                return RedirectToAction("Logout");
            }
        }
        else {
            model = await _service.GetUserAsync(id);
        }
       
        UserProfileViewModel viewmodel = new()
        {
            Name = model.Name,
            EmailAddress = model.Email,
        };

        return View(viewmodel);
    }
}