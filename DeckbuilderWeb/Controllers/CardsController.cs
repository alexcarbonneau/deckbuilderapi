﻿using DeckbuilderWeb.Extensions;
using DeckbuilderWeb.Models.Queries;
using DeckbuilderWeb.Services;
using DeckbuilderWeb.ViewModels.Cards;
using Microsoft.AspNetCore.Mvc;

namespace DeckbuilderWeb.Controllers
{
    /// <summary>
    /// Card controller
    /// </summary>
    public class CardsController : Controller
    {
        private readonly ICardService _cardService;

        /// <summary>
        /// Default constructor
        /// </summary>
        public CardsController(ICardService service)
        {
            _cardService = service;
        }

        /// <summary>
        /// Details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Details(string id)
        {
            var model = await _cardService.GetAsync(id);
            var viewmodel = (model != null) ? new CardViewModel(model) : new CardViewModel();
            viewmodel.Rulings = await _cardService.GetRulingsAsync(id);
            return View(viewmodel);
        }

        /// <summary>
        /// Details for a card intented to be displayed as a modal
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> DetailsModal(string id)
        {
            var model = await _cardService.GetAsync(id);
            var viewmodel = (model != null) ? new CardViewModel(model) : new CardViewModel();
            viewmodel.Rulings = await _cardService.GetRulingsAsync(id);
            return PartialView("_DetailsModal", viewmodel);
        }
        /// <summary>
        /// Display the advanced search page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Cards/Search")]
        public IActionResult Search()
        {
            return View();
        }

        /// <summary>
        /// Search async for ajax advanced search
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [Route("Cards/SearchAsync")]
        public async Task<IActionResult> SearchAsync([FromBody] CardSearchQuery query)
        {
            var model = await _cardService.GetSearchAsync(query: query);
            var viewmodel = (model != null && model.TotalCards > 0) ? new CardListViewModel(model) : new CardListViewModel();
            return PartialView("_SearchResultsPartial", viewmodel);
        }

        /// <summary>
        /// Loads next page async for ajax advanced search
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [Route("Cards/NextPageAsync")]
        public async Task<IActionResult> NextPageAsync([FromBody] string query)
        {
            var model = await _cardService.GetSearchAsync(query: query);
            var viewmodel = (model != null && model.TotalCards > 0) ? new CardListViewModel(model) : new CardListViewModel();
            return PartialView("_SearchResultsPartial", viewmodel);
        }
    }
}