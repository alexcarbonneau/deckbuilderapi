﻿using DeckbuilderWeb.Extensions;
using DeckbuilderWeb.Models.Decks;
using DeckbuilderWeb.Models.Queries;
using DeckbuilderWeb.Services;
using DeckbuilderWeb.Mappers;
using DeckbuilderWeb.ViewModels.Decks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace DeckbuilderWeb.Controllers
{
    /// <summary>
    ///  Deck controller
    /// </summary>
    [Authorize]
    public class DecksController : Controller
    {
        private readonly IDeckbuilderService _deckBuilderService;
        private readonly ICardService _cardService;

        /// <summary>
        /// Default constructor
        /// </summary>
        public DecksController(IDeckbuilderService deckbuilderService, ICardService cardService)
        {
            _deckBuilderService = deckbuilderService;
            _cardService = cardService;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            var id = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault();
            var model = await _deckBuilderService.GetDecksAsync(user_id: id, includePrivate: true);
            var viewmodel = (model != null && model.TotalDecks > 0) ? new DeckListViewModel(model) : new DeckListViewModel();
            return View(viewmodel);
        }

        /// <summary>
        /// Details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Details(int id)
        {
            var model = await _deckBuilderService.GetDeckAsync(id);

            if (model.Id <= 0) return RedirectToAction("Index");
            var viewmodel = new DeckViewModel(model);
            return View(viewmodel);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            return PartialView();
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="deck"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromForm] Deck deck)
        {
            deck.UserId = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault();
            deck.MainBoard = new List<DeckCard>();
            deck.SideBoard = new List<DeckCard>();
            deck.MaybeBoard = new List<DeckCard>();
            deck.CoverImage = string.Empty;
            //deck.CoverImage = Deck.DefaultCoverImage; 

            int id = await _deckBuilderService.CreateDeckAsync(deck);
            return Redirect("/Decks/Details/" + id);
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(int id)
        {
            var model = await _deckBuilderService.GetDeckAsync(id);
            var viewmodel = (model != null) ? new DeckViewModel(model) : new DeckViewModel();
            return PartialView(viewmodel);
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <param name="deck"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([FromForm] Deck deck)
        {
            var original = await _deckBuilderService.GetDeckAsync(deck.Id);
            original.Title = deck.Title;
            original.IsPrivate = deck.IsPrivate;
            original.IsPrototype = deck.IsPrototype;
            original.Format = deck.Format;
            original.Description = deck.Description;

            int id = await _deckBuilderService.EditDeckAsync(deck.Id, original);
            return Redirect("/Decks/Details/" + id);

            throw new NotImplementedException();
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Delete(int id)
        {
            var deck = await _deckBuilderService.GetDeckAsync(id);
            var userId = User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault();

            if (!deck.UserId.Equals(userId))
            {
                return Unauthorized();
            }

            var response = await _deckBuilderService.DeleteDeckAsync(id);
            if (response)
            {
                return RedirectToAction("Index");
            }

            return BadRequest();
        }

        /// <summary>
        /// Search async for ajax advanced search
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public async Task<IEnumerable<string>> AutocompleteAsync([FromBody] string query)
        {
            var response = await _cardService.AutocompleteAsync(query: query);
            return response.Data;
        }

        /// <summary>
        /// Add a card to the current deck
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> AddCardByName([FromBody] AjaxDeckCardQuery query)
        {
            var response = await _cardService.GetByNameAsync(query.CardName);

            if (response != null && query.DeckId > 0)
            {
                DeckCard card = new DeckCardMapper().Map(response);

                var model = await _deckBuilderService.AddCardToDeckAsync(card, query.DeckId);
                var viewmodel = (model != null) ? new DeckViewModel(model) : null;

                return viewmodel != null ? PartialView("_DeckBoardPartial", viewmodel) : NotFound();
            }

            else return NotFound();
        }

        /// <summary>
        /// Increment quantity of a card by one in a deck
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> AddOneToDeck([FromBody] AjaxDeckCardQuery query)
        {
            var model = await _deckBuilderService.AddOneToDeckAsync(query.CardId, query.DeckId);
            var viewmodel = (model != null) ? new DeckViewModel(model) : null;
            return viewmodel != null ? PartialView("_DeckBoardPartial", viewmodel) : NotFound();
        }

        /// <summary>
        /// Increment quantity of a card by one in a deck
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> RemoveOneFromDeck([FromBody] AjaxDeckCardQuery query)
        {
            var model = await _deckBuilderService.RemoveOneFromDeckAsync(query.CardId, query.DeckId);
            var viewmodel = (model != null) ? new DeckViewModel(model) : null;
            return viewmodel != null ? PartialView("_DeckBoardPartial", viewmodel) : NotFound();
        }

        /// <summary>
        /// Increment quantity of a card by one in a deck
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> SetQuantityInDeck([FromBody] AjaxDeckCardQuery query)
        {
            var model = await _deckBuilderService.SetQuantityInDeckAsync(query.CardId, query.DeckId, query.Quantity);
            var viewmodel = (model != null) ? new DeckViewModel(model) : null;
            return viewmodel != null ? PartialView("_DeckBoardPartial", viewmodel) : NotFound();
        }

        /// <summary>
        /// Increment quantity of a card by one in a deck
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> RemoveCardFromDeck([FromBody] AjaxDeckCardQuery query)
        {
            var model = await _deckBuilderService.RemoveCardFromDeckAsync(query.CardId, query.DeckId);
            var viewmodel = (model != null) ? new DeckViewModel(model) : null;
            return viewmodel != null ? PartialView("_DeckBoardPartial", viewmodel) : NotFound();
        }

        /// <summary>
        /// Increment quantity of a card by one in a deck
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> SetCardAsCommander([FromBody] AjaxDeckCardQuery query)
        {
            var model = await _deckBuilderService.SetCardAsCommanderAsync(query.CardId, query.DeckId);
            var viewmodel = (model != null) ? new DeckViewModel(model) : null;
            return viewmodel != null ? PartialView("_DeckBoardPartial", viewmodel) : NotFound();
        }

        /// <summary>
        /// Increment quantity of a card by one in a deck
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> ChangeCoverImage([FromBody] AjaxDeckCardQuery query)
        {
            var card = await _cardService.GetAsync(query.CardId);

            if (card is null) return NotFound();

            string imageUri;

            if (card.IsReversible)
            {
                imageUri = card.CardFaces.First().ImageUris.ArtCrop;
            }
            else
            {
                imageUri = card.ImageUris.ArtCrop;
            }

            var model = await _deckBuilderService.ChangeCoverImageAsync(imageUri, query.DeckId);
            var viewmodel = (model != null) ? new DeckViewModel(model) : null;
            return viewmodel != null ? PartialView("_DeckBannerPartial", viewmodel) : NotFound();
        }

        /// <summary>
        /// Refresh deck stats data form Ajax request
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetDeckStats(int id)
        {
            var model = await _deckBuilderService.GetDeckAsync(id);
            var viewmodel = new DeckViewModel(model);
            return PartialView("_DeckStatsPartial", viewmodel.MainBoard);
        }

        /// <summary>
        /// Export to text
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> ExportToText(int id)
        {
            var model = await _deckBuilderService.GetDeckAsync(id);
            var viewmodel = new DeckExportTextViewModel(model);

            return PartialView("_ExportTextPartial", viewmodel);
        }
    }
}