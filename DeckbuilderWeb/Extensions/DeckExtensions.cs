﻿using DeckbuilderWeb.Models.Decks;

namespace DeckbuilderWeb.Extensions
{
    /// <summary>
    /// Deck validator
    /// </summary>
    public static class DeckExtensions
    {
        /// <summary>
        /// Removes all cards with a quantity of zero or less in a deck
        /// </summary>
        /// <returns>Number of cards removed from the deck</returns>
        public static int Clean(this Deck deck) {

            int removedCards = 0;
            // Need to create a copy of the original deck on which to iterate and modify only the original
            var dirtyMainboard = deck.MainBoard.ToList(); 
            foreach (var card in dirtyMainboard) {
                if (card.Quantity < 1) {
                    deck.MainBoard.Remove(card);
                    removedCards++;
                }
            }

            return removedCards;
        }
    }
}
