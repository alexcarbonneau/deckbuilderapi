﻿$(document).ready(function () {
    $("#navbar-toggle").click(function () {
        $(this).find("i").toggleClass("fa-bars");
        $(this).find("i").toggleClass("fa-xmark");
        $("#navbar-drawer").toggleClass("opened");
        $("#navbar-overlay").toggleClass("visible");
        $("body").toggleClass("nav-opened");
    });
    
    $("#navbar-overlay").click(function () {
        $("#navbar-toggle i").addClass("fa-bars");
        $("#navbar-toggle i").removeClass("fa-xmark");
        $("#navbar-drawer").removeClass("opened");
        $(this).removeClass("visible");
        $("body").removeClass("nav-opened");
    });

    $(".modal").click(function (e) {
        if (e.target !== this)
            return;
        $(this).removeClass("visible");
    })

    $(".modal").on("click", ".modal-close, .modal-cancel", function () {
        $(this).closest(".modal").removeClass("visible");
    });

});