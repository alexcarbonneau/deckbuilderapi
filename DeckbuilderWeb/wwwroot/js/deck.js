﻿$(document).ready(function () {
    /* ============================ */
    /* VARIABLES                    */
    /* ============================ */
    var $form = $("#card-add-to-deck-form");
    var $cardName = $("#card-name");
    var $suggestions = $("#card-name-suggestions")
    var $deckBoards = $("#deck-boards");
    var $deckStats = $("#deck-stats");
    var $deckStatsContentChanged = $("#deck-stats-content-changed");
    var $banner = $("#deck-banner-container");
    var timeout = null;
    var timeoutValue = 800;

    /* ============================ */
    /* FUNCTIONS                    */
    /* ============================ */

    //TODO : Make card image fixed after a certain scrollTop and before reaching the end of the board

    function searchPreload(value) {
        if (value) {
            $suggestions.html("<div id='card-autocomplete-preloader' class='text-muted p-1'>Searching<i class='fa-solid fa-circle-notch fa-spin ml-1'></i></div>")
        } else {
            $suggestions.empty();
        }
    }

    function sanitizeTextField(string) {
        // [^0-9A-Za-z ']   --> All special characters except apostrophe
        let regex = /[^0-9A-Za-z ']/g;
        let sanitized = string.replaceAll(regex, '');
        return sanitized;
    }

    function autocompleteAsync() {
        let query = $cardName.val().trim();

        $.ajax({
            type: 'POST',
            url: '/Decks/Autocomplete',
            contentType: "application/json",
            data: JSON.stringify(sanitizeTextField(query)),
            success: function (data) {
                searchPreload(false);
                data.forEach(function (item) {
                    $suggestions.append("<div class='card-suggestions-item'>" + item + "</div>");
                });

                if (data == undefined || data.length < 1) {
                    $suggestions.append("<div class='p-1'>No results found.</div>");
                }
            },
            error: function (response) {
                searchPreload(false);
                $suggestions.html("An unexpected error has occured : " + response);
            },
            complete: function () {
            }
        });
    }

    function viewCardDetails(id) {
        let $cardDetailsModal = $("#card-details-modal");
        $.ajax({
            type: 'GET',
            url: '/Cards/DetailsModal/' + id,
            contentType: "application/json",
            success: function (data) {
                $cardDetailsModal.html(data);
                $cardDetailsModal.addClass("visible");
            },
            error: function (response) {
                alert("An unexpected error has occured. Please try again later.")
            },
        });
    }

    function exportToText(id) {
        let $exportDeckTextModal = $("#export-deck-text-modal");
        $.ajax({
            type: 'GET',
            url: '/Decks/ExportToText/' + id,
            contentType: "application/json",
            success: function (data) {
                $exportDeckTextModal.html(data);
                $exportDeckTextModal.addClass("visible");
            },
            error: function (response) {
                alert("An unexpected error has occured. Please try again later.")
            },
        });
    }

    function notifyDeckDataChanged() {
        $deckStatsContentChanged.slideDown("slow");
    }

    function addCardByName(name, id) {
        let body = {
            CardName: sanitizeTextField(name),
            DeckId: id
        }

        $.ajax({
            type: 'PUT',
            url: '/Decks/AddCardByName',
            contentType: "application/json",
            data: JSON.stringify(body),
            success: function (data) {
                $("#card-name").focus();
                $deckBoards.html(data);
                notifyDeckDataChanged();
            },
            error: function (response) {
                alert("An unexpected error has occured. Please try again later.")
            },
        });
    }

    function addOneToDeck(cardId, deckId) {
        let body = {
            CardId: cardId,
            DeckId: deckId
        }

        $.ajax({
            type: 'PUT',
            url: '/Decks/AddOneToDeck',
            contentType: "application/json",
            data: JSON.stringify(body),
            success: function (data) {
                $deckBoards.html(data);
                notifyDeckDataChanged();
            },
            error: function (response) {
                alert("An unexpected error has occured. Please try again later.")
            },
        });
    }

    function removeOneFromDeck(cardId, deckId) {
        let body = {
            CardId: cardId,
            DeckId: deckId
        }

        $.ajax({
            type: 'PUT',
            url: '/Decks/RemoveOneFromDeck',
            contentType: "application/json",
            data: JSON.stringify(body),
            success: function (data) {
                $deckBoards.html(data);
                notifyDeckDataChanged();
            },
            error: function (response) {
                alert("An unexpected error has occured. Please try again later.")
            },
        });
    }

    function setQuantity(cardId, deckId) {
        let input = prompt("How many do you want?");

        if (input === null || input == "") { return; }

        let quantity = parseInt(input);

        if (isNaN(quantity) || quantity < 0) {
            alert("Invalid input. Please enter a number greater than or equal to 0.");
            return;
        }

        let body = {
            CardId: cardId,
            DeckId: deckId,
            Quantity: quantity,
        }

        $.ajax({
            type: 'PUT',
            url: '/Decks/SetQuantityInDeck',
            contentType: "application/json",
            data: JSON.stringify(body),
            success: function (data) {
                $deckBoards.html(data);
                notifyDeckDataChanged();
            },
            error: function (response) {
                alert("An unexpected error has occured. Please try again later.")
            },
        });
    }

    function removeAllFromDeck(cardId, deckId) {
        let body = {
            CardId: cardId,
            DeckId: deckId
        }

        $.ajax({
            type: 'PUT',
            url: '/Decks/RemoveCardFromDeck',
            contentType: "application/json",
            data: JSON.stringify(body),
            success: function (data) {
                $deckBoards.html(data);
                notifyDeckDataChanged();
            },
            error: function (response) {
                alert("An unexpected error has occured. Please try again later.")
            },
        });
    }

    function setAsCommander(cardId, deckId) {
        let body = {
            CardId: cardId,
            DeckId: deckId
        }

        $.ajax({
            type: 'PUT',
            url: '/Decks/SetCardAsCommander',
            contentType: "application/json",
            data: JSON.stringify(body),
            success: function (data) {
                $deckBoards.html(data);
            },
            error: function (response) {
                alert("An unexpected error has occured. Please try again later.")
            },
        });
    }

    function useAsCoverImage(cardId, deckId) {
        let body = {
            CardId: cardId,
            DeckId: deckId
        }

        $.ajax({
            type: 'PUT',
            url: '/Decks/ChangeCoverImage',
            contentType: "application/json",
            data: JSON.stringify(body),
            success: function (data) {
                $banner.html(data);
            },
            error: function (response) {
                alert("An unexpected error has occured. Please try again later.")
            },
        });
    }

    function refreshDeckStats(deckId) {
        $.ajax({
            type: 'GET',
            url: '/Decks/GetDeckStats/' + deckId,
            contentType: "application/json",
            success: function (data) {
                $deckStats.html(data);
                $deckStatsContentChanged.slideUp("slow");
            },
            error: function (response) {
                alert("An unexpected error has occured. Please try again later.")
            },
        });
    }

    function copyDeckToClipBoard() {
        var range = document.createRange();
        range.selectNode(document.getElementById("deck-list-export-table"));
        window.getSelection().removeAllRanges(); // clear current selection
        window.getSelection().addRange(range); // to select text
        document.execCommand("copy");
        window.getSelection().removeAllRanges();// to deselect
    }

    /* ============================ */
    /* EVENT BINDING                */
    /* ============================ */
    $deckBoards.on("mouseenter", ".deck-board-table-item", function () {
        $("#card-preview .card-image img").attr("src", $(this).data("card-image"))
    })

    $form.on("keyup change paste", "#card-name", function () {
        if ($cardName.val() && $cardName.val().trim().length > 2) {
            $suggestions.show();
            searchPreload(true);
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                if ($cardName.val()) {

                    autocompleteAsync();
                }
            }, timeoutValue);
        } else {
            searchPreload(false);
            $suggestions.hide();
        }
    });

    $form.on("submit", function (e) {
        e.preventDefault();
        e.stopPropagation();
    })

    $cardName.on("focusout", function () {
        $cardName.val("");
        $suggestions.hide();
    })

    $("body").on("mousedown", ".card-suggestions-item", function () {
        let name = $(this).html();
        let id = $form.find("#deck-id").val();
        addCardByName(name, id);
    });

    $("#analyse-deck-trigger").click(function () {
        let deckId = $(this).data("id");
        refreshDeckStats(deckId);
    })

    $("#deck-export-text").click(function () {
        let deckId = $(this).data("id");
        exportToText(deckId);
    });

    $("body").on("click", "#deck-copy-trigger", function () {
        copyDeckToClipBoard();
        $("#deck-copy-confirmation").slideDown();
    });

    /* ============================ */
    /* CONTEXT MENU                 */
    /* ============================ */
    $(function () {
        $.contextMenu({
            selector: '.deck-board-table-context',
            trigger: 'left',
            items: {
                "detail": {
                    name: "View details",
                    icon: "fa-align-justify",
                    accesskey: 'd',
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        viewCardDetails(cardId);
                    }
                },
                "add_one": {
                    name: "Add one",
                    icon: "fa-plus",
                    accesskey: 'a',
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        let deckId = $form.find("#deck-id").val();
                        addOneToDeck(cardId, deckId);
                    }
                },
                "rem_one": {
                    name: "Remove one",
                    icon: "fa-minus",
                    accesskey: 'r',
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        let deckId = $form.find("#deck-id").val();
                        removeOneFromDeck(cardId, deckId);
                    }
                },
                "set_qty": {
                    name: "Set quantity",
                    icon: "fa-plus-minus",
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        let deckId = $form.find("#deck-id").val();
                        setQuantity(cardId, deckId);
                    }
                },
                "use_commander": {
                    name: "Use as commander",
                    icon: "fa-crown",
                    accesskey: "c",
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        let deckId = $form.find("#deck-id").val();
                        setAsCommander(cardId, deckId);
                    }
                },
                "use_as_cover_image": {
                    name: "Change cover image",
                    icon: "fa-photo",
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        let deckId = $form.find("#deck-id").val();
                        useAsCoverImage(cardId, deckId);
                    }
                },
                "rem_all": {
                    name: "Remove from deck",
                    icon: "fa-trash",
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        let deckId = $form.find("#deck-id").val();
                        removeAllFromDeck(cardId, deckId);
                    }
                },
            }
        });
    });

    $(function () {
        $.contextMenu({
            selector: '.deck-board-table-item',
            trigger: 'right',
            items: {
                "detail": {
                    name: "View details",
                    icon: "fa-align-justify",
                    accesskey: 'd',
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        viewCardDetails(cardId);
                    }
                },
                "add_one": {
                    name: "Add one",
                    icon: "fa-plus",
                    accesskey: 'a',
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        let deckId = $form.find("#deck-id").val();
                        addOneToDeck(cardId, deckId);
                    }
                },
                "rem_one": {
                    name: "Remove one",
                    icon: "fa-minus",
                    accesskey: 'r',
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        let deckId = $form.find("#deck-id").val();
                        removeOneFromDeck(cardId, deckId);
                    }
                },
                "set_qty": {
                    name: "Set quantity",
                    icon: "fa-plus-minus",
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        let deckId = $form.find("#deck-id").val();
                        setQuantity(cardId, deckId);
                    }
                },
                "use_commander": {
                    name: "Use as commander",
                    icon: "fa-crown",
                    accesskey: "c",
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        let deckId = $form.find("#deck-id").val();
                        setAsCommander(cardId, deckId);
                    }
                },
                "use_as_cover_image": {
                    name: "Change cover image",
                    icon: "fa-photo",
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        let deckId = $form.find("#deck-id").val();
                        useAsCoverImage(cardId, deckId);
                    }
                },
                "rem_all": {
                    name: "Remove from deck",
                    icon: "fa-trash",
                    callback: function () {
                        let cardId = $(this).closest("tr").data("card-id")
                        let deckId = $form.find("#deck-id").val();
                        removeAllFromDeck(cardId, deckId);
                    }
                },
            }
        });
    });
});