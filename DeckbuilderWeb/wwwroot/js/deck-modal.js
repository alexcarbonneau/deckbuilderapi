﻿$(document).ready(function () {
    /* ============================ */
    /* CREATE DECK                  */
    /* ============================ */
    $createDeckTrigger = $("#create-deck-modal-trigger");
    $createDeckModal = $("#create-deck-modal");

    $createDeckTrigger.click(function () {
        $createDeckModal.addClass("visible");

    });

    /* ============================ */
    /* EDIT DECK                    */
    /* ============================ */
    editDeckModalTrigger = $("#edit-deck-modal-trigger");
    $editDeckModal= $("#edit-deck-modal");

    editDeckModalTrigger.click(function () {
        $editDeckModal.addClass("visible");
    });

    /* ============================ */
    /* DELETE DECK                  */
    /* ============================ */
    $deleteDeckModalTrigger = $("#delete-deck-modal-trigger");
    $deleteDeckModal = $("#delete-deck-modal");

    $deleteDeckModalTrigger.click(function () {
        $deleteDeckModal.addClass("visible");
    });
});