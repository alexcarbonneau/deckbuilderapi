﻿$(document).ready(function () {
    /* ============================ */
    /* VARIABLES                    */
    /* ============================ */
    var $form = $("#card-advanced-search-form");
    var timeout = null;
    var timeoutValue = 1500;
    var $results = $("#searchResultsContainer");
    var $errors = $("#form-errors");
    var $errorsMessage = $("#form-errors .message");

    /* ============================ */
    /* FUNCTIONS                    */
    /* ============================ */
    function validateForm(form) {
        $errors.fadeOut();
        if (!form.find("#name").val() &&
            !form.find("#card-type").val() &&
            !form.find("#format").val() &&
            !form.find("#oracle").val() &&
            !form.find("#commander").prop("checked") == true &&
            !form.find("#card-types-others").val() &&
            !form.find("#color-w")[0].checked &&
            !form.find("#color-u")[0].checked &&
            !form.find("#color-b")[0].checked &&
            !form.find("#color-r")[0].checked &&
            !form.find("#color-g")[0].checked &&
            !form.find("#color-c")[0].checked &&
            !form.find("#color-m")[0].checked &&
            !form.find("input[name='cmc']:checked").val()) {

            $errorsMessage.html("Please enter at least one search parameter");
            $errors.slideDown();
            return false
        }
        return true;
    }

    function toggleFormSubmit() {

        $form.find("input[type='submit']").each(function () {
            this.disabled = !this.disabled;
        });
    }

    function checkboxGroupToArray(id) {
        var checked = []
        $("#" + id + " input[type='checkbox']:checked").each(function () {
            checked.push($(this).val());
        });
        return checked;
    }

    function typesToArray() {
        var types = [];
        if ($form.find("#card-type").val()) {
            types.push(sanitizeTextField($form.find("#card-type").val()));
        }

        var otherTypes = []
        if ($form.find("#card-types-others").val()) {
            otherTypes = $form.find("#card-types-others").val().split(" ");
            otherTypes = otherTypes.map(s => sanitizeTextField(s));
        }
        return types.concat(otherTypes);
    }

    function sanitizeTextField(string) {
        // [^0-9A-Za-z ']  --> Matches all special characters except apostrophe
        let regex = /[^0-9A-Za-z ']/g;
        let sanitized = string.replaceAll(regex, '');
        return sanitized;
    }

    function submitFormAsync(form) {
        let body = {
            Name: sanitizeTextField(form.find("#name").val()),
            OracleText: sanitizeTextField(form.find("#oracle").val()),
            CardTypes: typesToArray(),
            Format: form.find("#format").val(),
            IsCommander: form.find("#commander").prop("checked"),
            OrderBy: form.find("#orderby").val(),
            OrderDirection: form.find("#dir").val(),
            Colors: checkboxGroupToArray("colors"),
            ColorMatchType: form.find("#color-match").val(),
            ManaValue: form.find("input[name='cmc']:checked").val()
        }

        $.ajax({
            type: 'POST',
            url: '/Cards/SearchAsync',
            contentType: "application/json",
            data: JSON.stringify(body),
            success: function (data) {
                $results.html(data);
                $('html, body').animate({
                    scrollTop: ($results.offset().top - 95)
                }, 500);
            },
            error: function (data) {
                $errorsMessage.html("An unexpected error has occured. Please try again.");
                console.log(data);
                $errors.slideDown();
            },
            complete: function () {
            }
        });
    }

    /* ============================ */
    /* EVENT BINDING                */
    /* ============================ */
    $form.submit(function (e) {
        e.preventDefault();
        e.stopPropagation();

        toggleFormSubmit();
        if (validateForm($form)) {
            submitFormAsync($form);
        }
        clearTimeout(timeout);
        timeout = setTimeout(function () { toggleFormSubmit(); }, timeoutValue);
     });

    $form.find("#color-c").change(function () {
        if (this.checked) {
            $form.find("#colors input[type='checkbox']:checked:not(#color-c)").each(function () {
                this.checked = false;
            });
        }
    });

    $form.find("#colors input[type='checkbox']:not(#color-c)").change(function () {
        if (this.checked) {
            $form.find("#color-c").each(function () {
                this.checked = false;
            });
        }
    });

    /**
     *  Uncomment to allow auto submit on change
     */
    //$form.on('keyup change paste', 'input, select, textarea', function () {
    //    clearTimeout(timeout);
    //    timeout = setTimeout(function () {
    //        if (validateForm($form)) {
    //            submitFormAsync($form);
    //        } 
    //    }, timeoutValue);
    //});

    $(document).on("submit", "#card-next-page-form", function (e) {
        let nextPage = $("#card-next-page-form").find("#next-page").val();
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            type: 'POST',
            url: '/Cards/NextPageAsync',
            contentType: "application/json",
            data: JSON.stringify(nextPage),
            success: function (data) {
                $results.html(data);
                $('html, body').animate({
                    scrollTop: ($results.offset().top - 95)
                }, 500);
            },
            error: function () {
                $errorsMessage.html("An unexpected error has occured. Please try again.")
                $errors.slideDown();
            },
            complete: function () {
            }
        });
    });

})
