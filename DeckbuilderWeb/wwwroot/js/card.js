﻿$(document).ready(function () {
    $(".card-face-toggle").click(function () {
        $(".card-image-face-front").toggleClass("hidden");
        $(".card-image-face-back").toggleClass("hidden");
        $(".card-details-face-front").toggleClass("hidden");
        $(".card-details-face-back").toggleClass("hidden");
    });
});