﻿using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models
{
    /// <summary>
    /// Base model
    /// </summary>
    public class BaseModel : IModel
    {
        /// <summary>
        /// Created at
        /// </summary>
        [JsonPropertyName("created_at")]
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// UpdatedAt
        /// </summary>
        [JsonPropertyName("updated_at")]
        public DateTime UpdatedAt { get; set; }
    }
}
