﻿

using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Cards
{
    /// <summary>
    /// Prices
    /// </summary>
    public class Prices
    {
        /// <summary>
        /// USD
        /// </summary>
        [JsonPropertyName("usd")]
        public string Usd { get; set; }

        /// <summary>
        /// USD Foil
        /// </summary>
        [JsonPropertyName("usd_foil")]
        public string UsdFoil { get; set; }

        /// <summary>
        /// USD Etched
        /// </summary>
        [JsonPropertyName("usd_etched")]
        public string UsdEtched { get; set; }

        /// <summary>
        /// Euro
        /// </summary>
        [JsonPropertyName("eur")]
        public string Eur { get; set; }

        /// <summary>
        /// Euro Foil
        /// </summary>
        [JsonPropertyName("eur_foil")]
        public string EurFoil { get; set; }

        /// <summary>
        /// Tix (MTGO)
        /// </summary>
        [JsonPropertyName("tix")]
        public string Tix { get; set; }
    }
}