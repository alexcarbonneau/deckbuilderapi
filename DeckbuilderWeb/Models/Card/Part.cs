﻿// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Cards {
    /// <summary>
    /// All part : Related objects such as Tokens and combo pieces
    /// </summary>
    public class Part
    {
        /// <summary>
        /// Object type (always "related_card")
        /// </summary>
        [JsonPropertyName("object")]
        public string Object { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        [JsonPropertyName("id")]
        public string Id { get; set; }

        /// <summary>
        /// Component : describes relation with the card
        /// </summary>
        [JsonPropertyName("component")]
        public string Component { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonPropertyName("name")]
        public string Name { get; set; }

        /// <summary>
        /// Typeline
        /// </summary>
        [JsonPropertyName("type_line")]
        public string TypeLine { get; set; }

        /// <summary>
        /// URI
        /// </summary>
        [JsonPropertyName("uri")]
        public string Uri { get; set; }
    }
}

