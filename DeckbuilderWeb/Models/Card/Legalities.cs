﻿

using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Cards
{
    /// <summary>
    /// Legalities
    /// </summary>
    public class Legalities
    {
        /// <summary>
        /// Standard
        /// </summary>
        [JsonPropertyName("standard")]
        public string Standard { get; set; }

        /// <summary>
        /// Future
        /// </summary>
        [JsonPropertyName("future")]
        public string Future { get; set; }

        /// <summary>
        /// Historic
        /// </summary>
        [JsonPropertyName("historic")]
        public string Historic { get; set; }

        /// <summary>
        /// Gladiator
        /// </summary>
        [JsonPropertyName("gladiator")]
        public string Gladiator { get; set; }

        /// <summary>
        /// Pioneer
        /// </summary>
        [JsonPropertyName("pioneer")]
        public string Pioneer { get; set; }

        /// <summary>
        /// Modern
        /// </summary>
        [JsonPropertyName("modern")]
        public string Modern { get; set; }

        /// <summary>
        /// Legacy
        /// </summary>
        [JsonPropertyName("legacy")]
        public string Legacy { get; set; }

        /// <summary>
        /// Pauper
        /// </summary>
        [JsonPropertyName("pauper")]
        public string Pauper { get; set; }

        /// <summary>
        /// Vintage
        /// </summary>
        [JsonPropertyName("vintage")]
        public string Vintage { get; set; }

        /// <summary>
        /// Standard
        /// </summary>
        [JsonPropertyName("penny")]
        public string Penny { get; set; }

        /// <summary>
        /// Commander
        /// </summary>
        [JsonPropertyName("commander")]
        public string Commander { get; set; }

        /// <summary>
        /// Brawl
        /// </summary>
        [JsonPropertyName("brawl")]
        public string Brawl { get; set; }

        /// <summary>
        /// Historicbrawl
        /// </summary>
        [JsonPropertyName("historicbrawl")]
        public string Historicbrawl { get; set; }

        /// <summary>
        /// Alchemy
        /// </summary>
        [JsonPropertyName("alchemy")]
        public string Alchemy { get; set; }

        /// <summary>
        /// Paupercommander
        /// </summary>
        [JsonPropertyName("paupercommander")]
        public string Paupercommander { get; set; }

        /// <summary>
        /// Duel
        /// </summary>
        [JsonPropertyName("duel")]
        public string Duel { get; set; }

        /// <summary>
        /// Oldschool
        /// </summary>
        [JsonPropertyName("oldschool")]
        public string Oldschool { get; set; }

        /// <summary>
        /// Premodern
        /// </summary>
        [JsonPropertyName("premodern")]
        public string Premodern { get; set; }
    }
}