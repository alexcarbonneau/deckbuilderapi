﻿using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Cards
{
    /// <summary>
    /// Card
    /// </summary>
    public class Card : IModel
    {
        /// <summary>
        /// Object type (Always "card")
        /// </summary>
        [JsonPropertyName("object")]
        public string Object { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        [JsonPropertyName("id")]
        public string Id { get; set; }

        /// <summary>
        /// Oracle ID
        /// </summary>
        [JsonPropertyName("oracle_id")]
        public string OracleId { get; set; }

        /// <summary>
        /// Multiverse IDs
        /// </summary>
        [JsonPropertyName("multiverse_ids")]
        public List<int> MultiverseIds { get; set; }

        /// <summary>
        /// MTG Online ID
        /// </summary>
        [JsonPropertyName("mtgo_id")]
        public int MtgoId { get; set; }

        /// <summary>
        /// TCGPlayer ID
        /// </summary>
        [JsonPropertyName("tcgplayer_id")]
        public int TcgplayerId { get; set; }

        /// <summary>
        /// CardMarketID
        /// </summary>
        [JsonPropertyName("cardmarket_id")]
        public int CardmarketId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonPropertyName("name")]
        public string Name { get; set; }

        /// <summary>
        /// Language
        /// </summary>
        [JsonPropertyName("lang")]
        public string Language { get; set; }

        /// <summary>
        /// Released at
        /// </summary>
        [JsonPropertyName("released_at")]
        public string ReleasedAt { get; set; }

        /// <summary>
        /// URI : A link to this card object on Scryfall’s API.
        /// </summary>
        [JsonPropertyName("uri")]
        public string Uri { get; set; }

        /// <summary>
        /// Scryfall URI : A link to this card’s permapage on Scryfall’s website.
        /// </summary>
        [JsonPropertyName("scryfall_uri")]
        public string ScryfallUri { get; set; }

        /// <summary>
        /// Layout
        /// </summary>
        [JsonPropertyName("layout")]
        public string Layout { get; set; }

        /// <summary>
        /// HighresImage : True if this card’s imagery is high resolution.
        /// </summary>
        [JsonPropertyName("highres_image")]
        public bool HighresImage { get; set; }

        /// <summary>
        /// ImageStatus
        /// </summary>
        [JsonPropertyName("image_status")]
        public string ImageStatus { get; set; }

        /// <summary>
        /// Image URIs
        /// </summary>
        [JsonPropertyName("image_uris")]
        public ImageUris ImageUris { get; set; }

        /// <summary>
        /// Mana cost
        /// </summary>
        [JsonPropertyName("mana_cost")]
        public string ManaCost { get; set; }

        /// <summary>
        /// Mana value
        /// </summary>
        [JsonPropertyName("cmc")]
        public float ManaValue { get; set; }

        /// <summary>
        /// Type line
        /// </summary>
        [JsonPropertyName("type_line")]
        public string TypeLine { get; set; }

        /// <summary>
        /// Oracle text
        /// </summary>
        [JsonPropertyName("oracle_text")]
        public string OracleText { get; set; }

        /// <summary>
        /// Colors
        /// </summary>
        [JsonPropertyName("colors")]
        public List<string> Colors { get; set; }
        
        /// <summary>
        /// Color indicator
        /// </summary>
        [JsonPropertyName("color_indicator")]
        public List<string> ColorIndicator { get; set; }

        /// <summary>
        /// Color identity
        /// </summary>
        [JsonPropertyName("color_identity")]
        public List<string> ColorIdentity { get; set; }

        /// <summary>
        /// Produced mana
        /// </summary>
        [JsonPropertyName("produced_mana")]
        public List<string> ProducedMana { get; set; }

        /// <summary>
        /// Power
        /// </summary>
        [JsonPropertyName("power")]
        public string Power { get; set; }

        /// <summary>
        /// Toughness
        /// </summary>
        [JsonPropertyName("toughness")]
        public string Toughness { get; set; }

        /// <summary>
        /// Keywords
        /// </summary>
        [JsonPropertyName("keywords")]
        public List<string> Keywords { get; set; }

        /// <summary>
        /// Card faces
        /// </summary>
        [JsonPropertyName("card_faces")]
        public List<CardFace> CardFaces { get; set; }

        /// <summary>
        /// Legalities : An object describing the legality of this card across play formats. Possible legalities are legal, not_legal, restricted, and banned.
        /// </summary>
        [JsonPropertyName("legalities")]
        public Legalities Legalities { get; set; }

        /// <summary>
        /// Games : A list of games that this card print is available in, paper, arena, and/or mtgo.
        /// </summary>
        [JsonPropertyName("games")]
        public List<string> Games { get; set; }

        /// <summary>
        /// Reserved : True if this card is on the Reserved List.
        /// </summary>
        [JsonPropertyName("reserved")]
        public bool Reserved { get; set; }

        /// <summary>
        /// Foil
        /// </summary>
        [JsonPropertyName("foil")]
        public bool Foil { get; set; }

        /// <summary>
        /// Non foil
        /// </summary>
        [JsonPropertyName("nonfoil")]
        public bool Nonfoil { get; set; }

        /// <summary>
        /// Finishes
        /// </summary>
        [JsonPropertyName("finishes")]
        public List<string> Finishes { get; set; }

        /// <summary>
        /// Oversized
        /// </summary>
        [JsonPropertyName("oversized")]
        public bool Oversized { get; set; }

        /// <summary>
        /// Promo
        /// </summary>
        [JsonPropertyName("promo")]
        public bool Promo { get; set; }

        /// <summary>
        /// Reprint
        /// </summary>
        [JsonPropertyName("reprint")]
        public bool Reprint { get; set; }

        /// <summary>
        /// Variation
        /// </summary>
        [JsonPropertyName("variation")]
        public bool Variation { get; set; }

        /// <summary>
        /// Set ID
        /// </summary>
        [JsonPropertyName("set_id")]
        public string SetId { get; set; }

        /// <summary>
        /// Set code
        /// </summary>
        [JsonPropertyName("set")]
        public string SetCode { get; set; }

        /// <summary>
        /// Set name
        /// </summary>
        [JsonPropertyName("set_name")]
        public string SetName { get; set; }

        /// <summary>
        /// Set type
        /// </summary>
        [JsonPropertyName("set_type")]
        public string SetType { get; set; }

        /// <summary>
        /// Set URI
        /// </summary>
        [JsonPropertyName("set_uri")]
        public string SetUri { get; set; }

        /// <summary>
        /// Set search URI
        /// </summary>
        [JsonPropertyName("set_search_uri")]
        public string SetSearchUri { get; set; }

        /// <summary>
        /// Scryfall set URI
        /// </summary>
        [JsonPropertyName("scryfall_set_uri")]
        public string ScryfallSetUri { get; set; }

        /// <summary>
        /// Rulings URI
        /// </summary>
        [JsonPropertyName("rulings_uri")]
        public string RulingsUri { get; set; }

        /// <summary>
        /// Prints search URI
        /// </summary>
        [JsonPropertyName("prints_search_uri")]
        public string PrintsSearchUri { get; set; }

        /// <summary>
        /// CollectorNumber
        /// </summary>
        [JsonPropertyName("collector_number")]
        public string CollectorNumber { get; set; }

        /// <summary>
        /// Digital
        /// </summary>
        [JsonPropertyName("digital")]
        public bool Digital { get; set; }

        /// <summary>
        /// Rarity
        /// </summary>
        [JsonPropertyName("rarity")]
        public string Rarity { get; set; }

        /// <summary>
        /// Card back ID
        /// </summary>
        [JsonPropertyName("card_back_id")]
        public string CardBackId { get; set; }

        /// <summary>
        /// Artist
        /// </summary>
        [JsonPropertyName("artist")]
        public string Artist { get; set; }

        /// <summary>
        /// Artists IDs
        /// </summary>
        [JsonPropertyName("artist_ids")]
        public List<string> ArtistIds { get; set; }

        /// <summary>
        /// Illustration ID
        /// </summary>
        [JsonPropertyName("illustration_id")]
        public string IllustrationId { get; set; }

        /// <summary>
        /// Border color
        /// </summary>
        [JsonPropertyName("border_color")]
        public string BorderColor { get; set; }

        /// <summary>
        /// Frame
        /// </summary>
        [JsonPropertyName("frame")]
        public string Frame { get; set; }

        /// <summary>
        /// Frame effects
        /// </summary>
        [JsonPropertyName("frame_effects")]
        public List<string> FrameEffects { get; set; }

        /// <summary>
        /// Security stamp
        /// </summary>
        [JsonPropertyName("security_stamp")]
        public string SecurityStamp { get; set; }

        /// <summary>
        /// Full art
        /// </summary>
        [JsonPropertyName("full_art")]
        public bool FullArt { get; set; }

        /// <summary>
        /// Textless
        /// </summary>
        [JsonPropertyName("textless")]
        public bool Textless { get; set; }

        /// <summary>
        /// Booster : Whether this card is found in boosters.
        /// </summary>
        [JsonPropertyName("booster")]
        public bool Booster { get; set; }

        /// <summary>
        /// Story spotlight : True if this card is a Story Spotlight.
        /// </summary>
        [JsonPropertyName("story_spotlight")]
        public bool StorySpotlight { get; set; }

        /// <summary>
        /// EDHREC Rank
        /// </summary>
        [JsonPropertyName("edhrec_rank")]
        public int EdhrecRank { get; set; }

        /// <summary>
        /// Prices: An object containing daily price information for this card, including usd, usd_foil, usd_etched, eur, and tix prices, as strings.
        /// </summary>
        [JsonPropertyName("prices")]
        public Prices Prices { get; set; }

        /// <summary>
        /// Related URIs
        /// </summary>
        [JsonPropertyName("related_uris")]
        public RelatedUris RelatedUris { get; set; }

        /// <summary>
        /// PurchaseURIs
        /// </summary>
        [JsonPropertyName("purchase_uris")]
        public PurchaseUris PurchaseUris { get; set; }

        /// <summary>
        /// Loyalty
        /// </summary>
        [JsonPropertyName("loyalty")]
        public string Loyalty { get; set; }

        /// <summary>
        /// Flavor text
        /// </summary>
        [JsonPropertyName("flavor_text")]
        public string FlavorText { get; set; }

        /// <summary>
        /// Flavor name
        /// </summary>
        [JsonPropertyName("flavor_name")]
        public string FlavorName { get; set; }

        /// <summary>
        /// Watermark
        /// </summary>
        [JsonPropertyName("watermark")]
        public string Watermark { get; set; }

        /// <summary>
        /// All parts if card is meld or similar
        /// </summary>
        [JsonPropertyName("all_parts")]
        public List<Part> AllParts { get; set; }

        /// <summary>
        /// True if card is multifaced
        /// </summary>
        public bool IsMultifaced
        {
            get
            {
                return Layout switch
                {
                    "split" => true,
                    "flip" => true,
                    "transform" => true,
                    "modal_dfc" => true,
                    "adventure" => true,
                    "double_faced_token" => true,
                    "reversible_card" => true,
                    "art_series" => true,
                    _ => false
                };
            }
        }

        /// <summary>
        /// True if card is reversible
        /// </summary>
        public bool IsReversible
        {
            get
            {
                return Layout switch
                {
                    "transform" => true,
                    "modal_dfc" => true,
                    "reversible_card" => true,
                    "double_faced_token" => true,
                    "art_series" => true,
                    _ => false
                };
            }
        }
    }
}