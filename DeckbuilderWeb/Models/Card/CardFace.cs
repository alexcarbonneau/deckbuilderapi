﻿using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Cards
{
    /// <summary>
    /// Card face
    /// </summary>
    public class CardFace :IModel
    {
        /// <summary>
        /// Object type (always "card_face")
        /// </summary>
        [JsonPropertyName("object")]
        public string Object { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonPropertyName("name")]
        public string Name { get; set; }

        /// <summary>
        /// Image URIs
        /// </summary>
        [JsonPropertyName("image_uris")]
        public ImageUris ImageUris { get; set; }

        /// <summary>
        /// Mana cost
        /// </summary>
        [JsonPropertyName("mana_cost")]
        public string ManaCost { get; set; }

        /// <summary>
        /// Type line
        /// </summary>
        [JsonPropertyName("type_line")]
        public string TypeLine { get; set; }

        /// <summary>
        /// Oracle text
        /// </summary>
        [JsonPropertyName("oracle_text")]
        public string OracleText { get; set; }

        /// <summary>
        /// Colors
        /// </summary>
        [JsonPropertyName("colors")]
        public List<string> Colors { get; set; }

        /// <summary>
        /// Color indicator
        /// </summary>
        [JsonPropertyName("color_indicator")]
        public List<string> ColorIndicator { get; set; }

        /// <summary>
        /// Power
        /// </summary>
        [JsonPropertyName("power")]
        public string Power { get; set; }

        /// <summary>
        /// Toughness
        /// </summary>
        [JsonPropertyName("toughness")]
        public string Toughness { get; set; }

        /// <summary>
        /// Artist
        /// </summary>
        [JsonPropertyName("artist")]
        public string Artist { get; set; }

        /// <summary>
        /// Artist ID
        /// </summary>
        [JsonPropertyName("artist_id")]
        public string ArtistId { get; set; }

        /// <summary>
        /// Illustration ID
        /// </summary>
        [JsonPropertyName("illustration_id")]
        public string IllustrationId { get; set; }

        /// <summary>
        /// Loyalty
        /// </summary>
        [JsonPropertyName("loyalty")]
        public string Loyalty { get; set; }

        /// <summary>
        /// Flavor text
        /// </summary>
        [JsonPropertyName("flavor_text")]
        public string FlavorText { get; set; }

        /// <summary>
        /// Flavor name
        /// </summary>
        [JsonPropertyName("flavor_name")]
        public string FlavorName { get; set; }
    }
}