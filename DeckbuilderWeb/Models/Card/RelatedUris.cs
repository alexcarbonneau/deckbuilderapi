﻿using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Cards
{
    /// <summary>
    /// Related URIs
    /// </summary>
    public class RelatedUris
    {
        /// <summary>
        /// Gatherer
        /// </summary>
        [JsonPropertyName("gatherer")]
        public string Gatherer { get; set; }

        /// <summary>
        /// TCGpLayer Infinite Articles
        /// </summary>
        [JsonPropertyName("tcgplayer_infinite_articles")]
        public string TcgplayerInfiniteArticles { get; set; }

        /// <summary>
        /// TCGplayer Infinite Decks
        /// </summary>
        [JsonPropertyName("tcgplayer_infinite_decks")]
        public string TcgplayerInfiniteDecks { get; set; }

        /// <summary>
        /// EDHREC
        /// </summary>
        [JsonPropertyName("edhrec")]
        public string Edhrec { get; set; }

        /// <summary>
        /// MTG Top 8
        /// </summary>
        [JsonPropertyName("mtgtop8")]
        public string Mtgtop8 { get; set; }
    }
}