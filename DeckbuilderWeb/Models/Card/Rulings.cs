﻿using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Cards
{
    /// <summary>
    /// Rulings
    /// </summary>
    public class Rulings
    {
        /// <summary>
        /// Object type (always "list")
        /// </summary>
        [JsonPropertyName("object")]
        public string Object { get; set; }

        /// <summary>
        /// Has more
        /// </summary>
        [JsonPropertyName("has_more")]
        public bool HasMore { get; set; }

        /// <summary>
        /// Rules
        /// </summary>
        [JsonPropertyName("data")]
        public List<Rule> Rules { get; set; }
    }

    /// <summary>
    /// Rule
    /// </summary>
    public class Rule
    {
        /// <summary>
        /// Object type (always "ruling")
        /// </summary>
        [JsonPropertyName("object")]
        public string Object { get; set; }

        /// <summary>
        /// OracleId
        /// </summary>
        [JsonPropertyName("oracle_id")]
        public string OracleId { get; set; }

        /// <summary>
        /// Source of the rule (always "wotc" or "scryfall)
        /// </summary>
        [JsonPropertyName("source")]
        public string Source { get; set; }

        /// <summary>
        /// Published at : The date when the ruling or note was published.
        /// </summary>
        [JsonPropertyName("published_at")]
        public DateTime PublishedAt { get; set; }
        
        /// <summary>
        /// Comment : The text of the ruling.
        /// </summary>
        [JsonPropertyName("comment")]
        public string Comment { get; set; }
    }
}
