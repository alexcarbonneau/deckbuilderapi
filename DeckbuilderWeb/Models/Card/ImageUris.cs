﻿using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Cards
{
    /// <summary>
    /// Image URIs
    /// </summary>
    public class ImageUris
    {
        /// <summary>
        /// Small
        /// </summary>
        [JsonPropertyName("small")]
        public string Small { get; set; }

        /// <summary>
        /// Normal
        /// </summary>
        [JsonPropertyName("normal")]
        public string Normal { get; set; }

        /// <summary>
        /// Large
        /// </summary>
        [JsonPropertyName("large")]
        public string Large { get; set; }

        /// <summary>
        /// High quality transparent PNG
        /// </summary>
        [JsonPropertyName("png")]
        public string Png { get; set; }

        /// <summary>
        /// Art crop
        /// </summary>
        [JsonPropertyName("art_crop")]
        public string ArtCrop { get; set; }

        /// <summary>
        /// Border crop
        /// </summary>
        [JsonPropertyName("border_crop")]
        public string BorderCrop { get; set; }
    }
}
