﻿using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Cards
{
    /// <summary>
    /// Card list
    /// </summary>
    public class CardList
    {
        /// <summary>
        /// Object type (always "list")
        /// </summary>
        [JsonPropertyName("object")]
        public string Object { get; set; }

        /// <summary>
        /// Total cards : This field will contain the total number of cards found across all pages.
        /// </summary>
        [JsonPropertyName("total_cards")]
        public int TotalCards { get; set; }

        /// <summary>
        /// Has more : True if this List is paginated and there is a page beyond the current page.
        /// </summary>
        [JsonPropertyName("has_more")]
        public bool HasMore { get; set; }

        /// <summary>
        /// Next page : If there is a page beyond the current page, this field will contain a full API URI to that page.
        /// You may submit a HTTP GET request to that URI to continue paginating forward on this List.
        /// </summary>
        [JsonPropertyName("next_page")]
        public string NextPage { get; set; }

        /// <summary>
        /// Data
        /// </summary>
        [JsonPropertyName("data")]
        public List<Card> Cards { get; set; }
    }
}
