﻿

using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Cards
{
    /// <summary>
    /// Purchase URIs
    /// </summary>
    public class PurchaseUris
    {
        /// <summary>
        /// TCG Player
        /// </summary>
        [JsonPropertyName("tcgplayer")]
        public string Tcgplayer { get; set; }

        /// <summary>
        /// Cardmarket
        /// </summary>
        [JsonPropertyName("cardmarket")]
        public string Cardmarket { get; set; }

        /// <summary>
        /// Cardhoarder
        /// </summary>
        [JsonPropertyName("cardhoarder")]
        public string Cardhoarder { get; set; }
    }
}