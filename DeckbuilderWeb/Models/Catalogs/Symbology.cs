﻿using System.Text.RegularExpressions;

namespace DeckbuilderWeb.Models.Catalogs
{
    /// <summary>
    /// Symbology
    /// </summary>
    public static class Symbology
    {
        private const string _baseUri = "https://c2.scryfall.com/file/scryfall-symbols/card-symbols/";

        /// <summary>
        /// Parses a bracket-formatted string of symbols into svg images URIs
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static IEnumerable<string> ParseSymbols(string text)
        {
            // Regex : Matches contents between brackets. Ex : {2}{U}{B} matches "2", "U", and "B"
            var symbols = Regex.Split(text, "{(.*?)}").Where(s=> s != string.Empty).ToList();
            // Regex will leave empty strings in array
            //symbols.RemoveAll(s => s == "");

            var symbolSVGs = new List<string>();
            foreach (string s in symbols)
            {
                symbolSVGs.Add(GetSvgForSymbol(s));
            }

            return symbolSVGs;
        }

        /// <summary>
        /// Gets the url to an svg MTG symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public static string GetSvgForSymbol(string symbol)
        {
            return symbol switch
            {
                "T" => _baseUri + "T.svg",
                "Q" => _baseUri + "Q.svg",
                "E" => _baseUri + "E.svg",
                "PW" => _baseUri + "PW.svg",
                "CHAOS" => _baseUri + "CHAOS.svg",
                "A" => _baseUri + "A.svg",
                "X" => _baseUri + "X.svg",
                "Y" => _baseUri + "Y.svg",
                "Z" => _baseUri + "Z.svg",
                "0" => _baseUri + "0.svg",
                "½" => _baseUri + "HALF.svg",
                "1" => _baseUri + "1.svg",
                "2" => _baseUri + "2.svg",
                "3" => _baseUri + "3.svg",
                "4" => _baseUri + "4.svg",
                "5" => _baseUri + "5.svg",
                "6" => _baseUri + "6.svg",
                "7" => _baseUri + "7.svg",
                "8" => _baseUri + "8.svg",
                "9" => _baseUri + "9.svg",
                "10" => _baseUri + "10.svg",
                "11" => _baseUri + "11.svg",
                "12" => _baseUri + "12.svg",
                "13" => _baseUri + "13.svg",
                "14" => _baseUri + "14.svg",
                "15" => _baseUri + "15.svg",
                "16" => _baseUri + "16.svg",
                "17" => _baseUri + "17.svg",
                "18" => _baseUri + "18.svg",
                "19" => _baseUri + "19.svg",
                "20" => _baseUri + "20.svg",
                "100" => _baseUri + "100.svg",
                "1000000" => _baseUri + "1000000.svg",
                "∞" => _baseUri + "INFINITY.svg",
                "W/U" => _baseUri + "WU.svg",
                "W/B" => _baseUri + "WB.svg",
                "B/R" => _baseUri + "BR.svg",
                "B/G" => _baseUri + "BG.svg",
                "U/B" => _baseUri + "UB.svg",
                "U/R" => _baseUri + "UR.svg",
                "R/G" => _baseUri + "RG.svg",
                "R/W" => _baseUri + "RW.svg",
                "G/W" => _baseUri + "GW.svg",
                "G/U" => _baseUri + "GU.svg",
                "B/G/P" => _baseUri + "BGP.svg",
                "B/R/P" => _baseUri + "BRP.svg",
                "G/U/P" => _baseUri + "GUP.svg",
                "G/W/P" => _baseUri + "GWP.svg",
                "R/G/P" => _baseUri + "RGP.svg",
                "R/W/P" => _baseUri + "RWP.svg",
                "U/B/P" => _baseUri + "UBP.svg",
                "U/R/P" => _baseUri + "URP.svg",
                "W/B/P" => _baseUri + "WBP.svg",
                "W/U/P" => _baseUri + "WUP.svg",
                "2/W" => _baseUri + "2W.svg",
                "2/U" => _baseUri + "2U.svg",
                "2/B" => _baseUri + "2B.svg",
                "2/R" => _baseUri + "2R.svg",
                "2/G" => _baseUri + "2G.svg",
                "P" => _baseUri + "P.svg",
                "W/P" => _baseUri + "WP.svg",
                "U/P" => _baseUri + "UP.svg",
                "B/P" => _baseUri + "BP.svg",
                "R/P" => _baseUri + "RP.svg",
                "G/P" => _baseUri + "GP.svg",
                "HW" => _baseUri + "HW.svg",
                "HR" => _baseUri + "HR.svg",
                "W" => _baseUri + "W.svg",
                "U" => _baseUri + "U.svg",
                "B" => _baseUri + "B.svg",
                "R" => _baseUri + "R.svg",
                "G" => _baseUri + "G.svg",
                "C" => _baseUri + "C.svg",
                "S" => _baseUri + "S.svg",
                "M" => "/assets/images/M.svg",
                "//" => "//",
                _ => ""
            };
        }
    }
}
