﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Decks
{
    /// <summary>
    /// Deck
    /// </summary>
    public class Deck : BaseModel
    {
        /// <summary>
        /// Id
        /// </summary>
        [JsonPropertyName("id")]
        public int Id { get; set; }

        /// <summary>
        /// UserId 
        /// </summary>
        [JsonPropertyName("user_id")]
        public string UserId { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        [MaxLength(100)]
        [JsonPropertyName("title")]
        public string Title { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        [MaxLength(1000)]
        [JsonPropertyName("description")]
        public string? Description { get; set; }

        /// <summary>
        /// Cover image
        /// </summary>
        [JsonPropertyName("cover_image")]
        public string CoverImage { get; set; }

        /// <summary>
        /// Format 
        /// </summary>
        [MaxLength(100)]
        [JsonPropertyName("format")]
        public string Format { get; set; }

        /// <summary>
        /// True if deck shouldn't appear in public lists
        /// </summary>
        [JsonPropertyName("is_private")]
        public bool IsPrivate { get; set; }

        /// <summary>
        /// True if deck doesn't need validation
        /// </summary>
        [JsonPropertyName("is_prototype")]
        public bool IsPrototype { get; set; }

        /// <summary>
        /// List of cards in the deck's main board
        /// </summary>
        [JsonPropertyName("main_board")]
        public List<DeckCard> MainBoard { get; set; }
        /// <summary>
        /// List of cards in the deck's side board
        /// </summary>
        [JsonPropertyName("side_board")]
        public List<DeckCard> SideBoard { get; set; }

        /// <summary>
        /// List of cards in the deck's maybe board
        /// </summary>
        [JsonPropertyName("maybe_board")]
        public List<DeckCard> MaybeBoard { get; set; }

        /// <summary>
        /// Default deck cover image
        /// </summary>
        public static string DefaultCoverImage = "https://c1.scryfall.com/file/scryfall-cards/art_crop/front/5/5/55ac2ca9-b6cf-453a-9d8b-8b7be5695f1e.jpg?1641603730";
    }
}