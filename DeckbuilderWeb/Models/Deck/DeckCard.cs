﻿using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Decks
{
    /// <summary>
    /// Deck card
    /// </summary>
    public class DeckCard : IModel
    {
        /// <summary>
        /// Scryfall ID
        /// </summary>
        [JsonPropertyName("id")]
        public string Id { get; set; }

        /// <summary>
        /// Quantity in deck
        /// </summary>
        [JsonPropertyName("quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonPropertyName("name")]
        public string Name { get; set; }

        /// <summary>
        /// Mana cost formated with brackets
        /// </summary>
        [JsonPropertyName("mana_cost")]
        public string ManaCost { get; set; }

        /// <summary>
        /// Mana value
        /// </summary>
        [JsonPropertyName("cmc")]
        public float ManaValue { get; set; }

        /// <summary>
        /// Colors
        /// </summary>
        [JsonPropertyName("colors")]
        public List<string> Colors { get; set; }

        /// <summary>
        /// Color identity
        /// </summary>
        [JsonPropertyName("color_identity")]
        public List<string> ColorIdentity { get; set; }

        /// <summary>
        /// Produced mana
        /// </summary>
        [JsonPropertyName("produced_mana")]
        public List<string> ProducedMana { get; set; }

        /// <summary>
        /// Rarity
        /// </summary>
        [JsonPropertyName("rarity")]
        public string Rarity { get; set; }

        /// <summary>
        /// Type line
        /// </summary>
        [JsonPropertyName("type_line")]
        public string TypeLine { get; set; }

        /// <summary>
        /// Set code ( 3 letters)
        /// </summary>
        [JsonPropertyName("set")]
        public string SetCode { get; set; }

        /// <summary>
        /// Set full name
        /// </summary>
        [JsonPropertyName("set_name")]
        public string SetName { get; set; }

        /// <summary>
        /// Image of front face
        /// </summary>
        [JsonPropertyName("image_front")]
        public string ImageFront { get; set; }

        /// <summary>
        /// Image of back face. Null on monofaced cards
        /// </summary>
        [JsonPropertyName("image_back")]
        public string? ImageBack { get; set; }

        /// <summary>
        /// True if this card is a commander
        /// </summary>
        [JsonPropertyName("is_commander")]
        public bool IsCommander { get; set; }

        /// <summary>
        /// True if this card is a companion
        /// </summary>
        [JsonPropertyName("is_companion")]
        public bool IsCompanion { get; set; }
    }
}