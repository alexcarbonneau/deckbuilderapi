﻿using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Decks
{
    /// <summary>
    /// Deck list
    /// </summary>
    public class DeckList : IModel
    {
        /// <summary>
        /// Total cards
        /// </summary>
        [JsonPropertyName("total_decks")]
        public int TotalDecks { get => Decks.Count; }

        /// <summary>
        /// Decks
        /// </summary>
        [JsonPropertyName("decks")]
        public List<Deck> Decks { get; set; }
    }
}
