﻿using Microsoft.AspNetCore.Mvc;

namespace DeckbuilderWeb.Models.Queries
{
    /// <summary>
    /// Query structure of an operation to apply to a deck from JSON
    /// </summary>
    [BindProperties]
    public class AjaxDeckCardQuery
    {
        /// <summary>
        /// Card name
        /// </summary>
        public string CardName { get; set; }

        /// <summary>
        /// Card ID
        /// </summary>
        public string CardId { get; set; }

        /// <summary>
        /// ID of the deck the card will be added to
        /// </summary>
        public int DeckId { get; set; }

        /// <summary>
        /// Quantity
        /// </summary>
        public int Quantity { get; set; }
    }
}