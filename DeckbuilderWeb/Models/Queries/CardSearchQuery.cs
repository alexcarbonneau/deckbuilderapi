﻿using Microsoft.AspNetCore.Mvc;

namespace DeckbuilderWeb.Models.Queries
{
    /// <summary>
    /// Card search query
    /// </summary>
    [BindProperties]
    public class CardSearchQuery
    {
        /// <summary>
        /// Card name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Card types
        /// </summary>
        public string[] CardTypes { get; set; }

        /// <summary>
        /// Format legality
        /// </summary>
        public string Format { get; set; }

        /// <summary>
        /// Oracle text
        /// </summary>
        public string OracleText { get; set; }

        /// <summary>
        /// True if searching for a commander
        /// </summary>
        public bool IsCommander { get; set; }
        
        /// <summary>
        /// Card colors (OR)
        /// </summary>
        public string[] Colors { get; set; }

        /// <summary>
        /// Defines the logical operator to apply to color search.
        /// </summary>
        public string ColorMatchType { get; set; }

        /// <summary>
        /// Mana value
        /// </summary>
        public string ManaValue { get; set; }

        /// <summary>
        /// Order by : Default value is "name"
        /// </summary>
        public string OrderBy { get; set; }

        /// <summary>
        /// Order by direction
        /// </summary>
        public string OrderDirection { get; set; }

        /// <summary>
        /// Returns the search query in a formatted string using Scryfall api's fulltext search system
        /// </summary>
        /// <returns></returns>
        public string ToScryfallQuery()
        {
            string query = "";

            if (!string.IsNullOrEmpty(Name))
            {
                query += Name;
            }

            if (!string.IsNullOrEmpty(OracleText)) {
                string[] words = OracleText.Split(' ');
                foreach (string word in words) {
                    query += "+o:" + word;
                }
            }

            query = AddColors(query);
            query = AddManaValue(query);
            query = AddCardTypes(query);

            if (!string.IsNullOrEmpty(Format))
            {
                query += "+f:" + Format;
            }

            if (IsCommander) {
                query += "+is:commander";
            }

            query += "+not:digital"; // Never include digital cards (cards available only on MTGO and MTGA)

            if (!string.IsNullOrEmpty(OrderBy))
            {
                query += "&order=" + OrderBy + "&dir=" + OrderDirection;
            }

            return query;
        }

        /// <summary>
        /// Add colors
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private string AddColors(string query) {
            if (Colors != null && Colors.Length > 0)
            {
                if (Colors.Contains("c"))
                {
                    query += "+" + ColorMatchType + "c";
                }
                else
                {
                    if (Colors.Length > 1)
                    {
                        query += "+" + ColorMatchType;
                        foreach (string color in Colors)
                        {
                            if (color != "m") { query += color; }
                        }

                        if (Colors.Contains("m"))
                        {
                            query += "+" + ColorMatchType + "m";
                        }
                    }
                    else
                    {
                        if (Colors[0] == "m")
                        {
                            query += "+" + ColorMatchType + "m";
                        }
                        else
                        {
                            query += "+" + ColorMatchType + Colors[0];
                        }
                    }
                }
            }
            return query;
        }

        /// <summary>
        /// Adds mana value to query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private string AddManaValue(string query) {
            if (!string.IsNullOrEmpty(ManaValue))
            {
                if (ManaValue == "+")
                {
                    query += "+mv>7";
                }
                else
                {
                    query += "+mv:" + ManaValue;
                }
            }
            return query;
        }

        /// <summary>
        /// Add card types
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private string AddCardTypes(string query) {
            if (CardTypes != null && CardTypes.Length > 0)
            {
                foreach (string type in CardTypes)
                {
                    query += "+t:" + type;
                }
            }
            return query;
        }
    }
}
