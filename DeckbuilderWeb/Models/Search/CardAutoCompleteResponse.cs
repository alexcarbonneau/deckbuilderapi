﻿using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Search
{
    /// <summary>
    /// Card auto complete response
    /// </summary>
    public class CardAutoCompleteResponse
    {
        /// <summary>
        /// Object type (always "catalog")
        /// </summary>
        [JsonPropertyName("object")]
        public string Object { get; set; }

        /// <summary>
        /// Total number of suggestions in Data
        /// </summary>
        [JsonPropertyName("total_values")]
        public int TotalValues { get; set; }

        /// <summary>
        /// Data
        /// </summary>
        [JsonPropertyName("data")]
        public List<string> Data { get; set; }
    }
}