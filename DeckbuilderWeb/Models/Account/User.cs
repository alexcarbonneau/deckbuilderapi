﻿using System.Text.Json.Serialization;

namespace DeckbuilderWeb.Models.Account
{
    /// <summary>
    /// User model
    /// </summary>
    public class User : BaseModel
    {
        /// <summary>
        /// Id
        /// </summary>
        [JsonPropertyName("id")]
        public string Id { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [JsonPropertyName("email")]
        public string Email { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonPropertyName("name")]
        public string Name { get; set; }

        /// <summary>
        /// Bio
        /// </summary>
        [JsonPropertyName("bio")]
        public string Bio { get; set; }
    }
}
