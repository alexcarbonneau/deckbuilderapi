﻿namespace DeckbuilderWeb.Mappers
{
    /// <summary>
    /// Mapper interface
    /// </summary>
    public interface IMapper<TEntity, TModel>
    {
        /// <summary>
        /// Map
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TModel Map(TEntity entity);
    }
}
