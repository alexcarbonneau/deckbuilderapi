﻿using DeckbuilderWeb.Mappers;
using DeckbuilderWeb.Models.Cards;
using DeckbuilderWeb.Models.Decks;

namespace DeckbuilderWeb.Mappers
{
    /// <summary>
    /// Scryfall to deck card converter
    /// </summary>
    public class DeckCardMapper : IMapper<Card, DeckCard>
    {
        /// <summary>
        /// Convert
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public DeckCard Map(Card entity)
        {
            DeckCard card = new()
            {
                Id = entity.Id,
                Quantity = 1,
                Name = entity.Name,
                ManaCost = entity.ManaCost,
                ManaValue = entity.ManaValue,
                Colors = entity.Colors,
                ColorIdentity = entity.ColorIdentity,
                ProducedMana = entity.ProducedMana,
                Rarity = entity.Rarity,
                TypeLine = entity.TypeLine,
                SetCode = entity.SetCode,
                SetName = entity.SetName,
            };

            if (entity.IsMultifaced && entity.IsReversible)
            {
                card.ImageFront = entity.CardFaces.First().ImageUris.Normal;
                card.ImageBack = entity.CardFaces.Last().ImageUris.Normal;
                card.ManaCost = entity.CardFaces.First().ManaCost;
            }
            else {
                card.ImageFront = entity.ImageUris.Normal;
            }

            return card;
        }
    }
}
