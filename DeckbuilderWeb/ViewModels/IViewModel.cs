﻿using DeckbuilderWeb.Models;

namespace DeckbuilderWeb.ViewModels
{
    /// <summary>
    /// IViewModel interface
    /// </summary>
    public interface IViewModel<T> where T : IModel
    {
        /// <summary>
        /// Convert ViewModel back to a model
        /// </summary>
        /// <returns></returns>
        public T GetModel();
    }
}
