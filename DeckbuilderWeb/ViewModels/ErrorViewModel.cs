namespace DeckbuilderWeb.ViewModels
{
    /// <summary>
    /// Error viewmodel
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        /// Request Id
        /// </summary>
        public string? RequestId { get; set; }

        /// <summary>
        /// Show request Id
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}