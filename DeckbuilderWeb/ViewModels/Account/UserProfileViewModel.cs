﻿namespace DeckbuilderWeb.ViewModels.Account
{
    /// <summary>
    /// User profile viewmodel
    /// </summary>
    public class UserProfileViewModel
    {
        /// <summary>
        /// Email address
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }
}
