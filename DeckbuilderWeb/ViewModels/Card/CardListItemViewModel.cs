﻿using DeckbuilderWeb.Models.Cards;

namespace DeckbuilderWeb.ViewModels.Cards
{
    /// <summary>
    /// Card list item viewmodel
    /// </summary>
    public class CardListItemViewModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Layout
        /// </summary>
        public string Layout { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Set code
        /// </summary>
        public string SetCode { get; set; }

        /// <summary>
        /// Set name
        /// </summary>
        public string SetName { get; set; }

        /// <summary>
        /// Rarity
        /// </summary>
        public string Rarity { get; set; }

        /// <summary>
        /// Prices
        /// </summary>
        public Prices Prices { get; set; }

        /// <summary>
        /// Prices
        /// </summary>
        public ImageUris ImageUris { get; set; }

        /// <summary>
        /// Card faces
        /// </summary>
        public List<CardFaceViewModel> CardFaces { get; set; }

        /// <summary>
        /// True if card is multifaced
        /// </summary>
        public bool IsMultifaced { get; set; }

        /// <summary>
        /// True if card is reversible
        /// </summary>
        public bool IsReversible { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CardListItemViewModel()
        {
        }

        /// <summary>
        /// Constructor from model
        /// </summary>
        /// <param name="model"></param>
        public CardListItemViewModel(Card model)
        {
            Id = model.Id;
            Layout = model.Layout;
            Name = model.Name;
            SetCode = model.SetCode;
            SetName = model.SetName;
            Rarity = model.Rarity;
            Prices = model.Prices;
            ImageUris = model.ImageUris;
            CardFaces = model.CardFaces?.Select(f => new CardFaceViewModel(f)).ToList();  
            IsMultifaced = model.IsMultifaced;
            IsReversible = model.IsReversible;
        }
    }
}