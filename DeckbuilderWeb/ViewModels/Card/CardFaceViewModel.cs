﻿using DeckbuilderWeb.Models.Cards;

namespace DeckbuilderWeb.ViewModels.Cards
{
    /// <summary>
    /// Card face viewmodel
    /// </summary>
    public class CardFaceViewModel : IViewModel<CardFace>
    {
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Image URIs
        /// </summary>
        public ImageUris ImageUris { get; set; }

        /// <summary>
        /// Mana cost
        /// </summary>
        public string ManaCost { get; set; }
     
        /// <summary>
        /// Type line
        /// </summary>
        public string TypeLine { get; set; }

        /// <summary>
        /// Oracle text
        /// </summary>
        public string OracleText { get; set; }

        /// <summary>
        /// Colors
        /// </summary>
        public List<string> Colors { get; set; }

        /// <summary>
        /// Color indicator
        /// </summary>
        public List<string> ColorIndicator { get; set; }

        /// <summary>
        /// Power
        /// </summary>
        public string Power { get; set; }

        /// <summary>
        /// Toughness
        /// </summary>
        public string Toughness { get; set; }

        /// <summary>
        /// Artist
        /// </summary>
        public string Artist { get; set; }

        /// <summary>
        /// Artist ID
        /// </summary>
        public string ArtistId { get; set; }

        /// <summary>
        /// Illustration ID
        /// </summary>
        public string IllustrationId { get; set; }

        /// <summary>
        /// Loyalty
        /// </summary>
        public string Loyalty { get; set; }

        /// <summary>
        /// Flavor text
        /// </summary>
        public string FlavorText { get; set; }

        /// <summary>
        /// Flavor name
        /// </summary>
        public string FlavorName { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CardFaceViewModel()
        {
        }

        /// <summary>
        /// Constructor from model
        /// </summary>
        /// <param name="model"></param>
        public CardFaceViewModel(CardFace model)
        {
            Name = model.Name;
            ImageUris = model.ImageUris;
            ManaCost = model.ManaCost;
            TypeLine = model.TypeLine;
            OracleText = model.OracleText;
            Colors = model.Colors;
            ColorIndicator = model.ColorIndicator;
            Power = model.Power;
            Toughness = model.Toughness;
            Artist = model.Artist;
            ArtistId = model.ArtistId;
            IllustrationId = model.IllustrationId;
            Loyalty = model.Loyalty;
            FlavorText = model.FlavorText;
            FlavorName = model.FlavorName;
        }

        /// <summary>
        /// Get model
        /// </summary>
        /// <returns></returns>
        public CardFace GetModel()
        {
            return new()
            {
                Name = Name,
                ImageUris = ImageUris,
                ManaCost = ManaCost,
                TypeLine = TypeLine,
                OracleText = OracleText,
                Colors = Colors,
                ColorIndicator = ColorIndicator,
                Power = Power,
                Toughness = Toughness,
                Artist = Artist,
                ArtistId = ArtistId,
                IllustrationId = ArtistId,
                Loyalty = Loyalty,
                FlavorText = FlavorText,
                FlavorName = FlavorName,
            };
        }
    }
}
