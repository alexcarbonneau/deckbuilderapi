﻿using DeckbuilderWeb.Models.Cards;
using Newtonsoft.Json.Linq;

namespace DeckbuilderWeb.ViewModels.Cards
{
    /// <summary>
    /// Card viewmodel
    /// </summary>
    public class CardViewModel : IViewModel<Card>
    {
        /// <summary>
        /// ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Oracle ID
        /// </summary>
        public string OracleId { get; set; }

        /// <summary>
        /// Oracle text
        /// </summary>
        public string OracleText { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// URI
        /// </summary>
        public string Uri { get; set; }

        /// <summary>
        /// Scryfall URI
        /// </summary>
        public string ScryfallUri { get; set; }

        /// <summary>
        /// Layout
        /// </summary>
        public string Layout { get; set; }

        /// <summary>
        /// Image URIs
        /// </summary>
        public ImageUris ImageUris { get; set; }

        /// <summary>
        /// Mana cost
        /// </summary>
        public string ManaCost { get; set; }

        /// <summary>
        /// Mana value
        /// </summary>
        public float ManaValue { get; set; }

        /// <summary>
        /// Type line
        /// </summary>
        public string TypeLine { get; set; }

        /// <summary>
        /// Colors
        /// </summary>
        public List<string> Colors { get; set; }

        /// <summary>
        /// Color indicator
        /// </summary>
        public List<string> ColorIndicator { get; set; }

        /// <summary>
        /// Color identity
        /// </summary>
        public List<string> ColorIdentity { get; set; }

        /// <summary>
        /// Produced mana
        /// </summary>
        public List<string> ProducedMana { get; set; }

        /// <summary>
        /// Power
        /// </summary>
        public string Power { get; set; }

        /// <summary>
        /// Toughness
        /// </summary>
        public string Toughness { get; set; }

        /// <summary>
        /// Keywords
        /// </summary>
        public List<string> Keywords { get; set; }

        /// <summary>
        /// Card faces
        /// </summary>
        public List<CardFaceViewModel> CardFaces { get; set; }

        /// <summary>
        /// Legalities
        /// </summary>
        public Legalities Legalities { get; set; }

        /// <summary>
        /// Set ID
        /// </summary>
        public string SetId { get; set; }

        /// <summary>
        /// Set code
        /// </summary>
        public string SetCode { get; set; }

        /// <summary>
        /// Set name
        /// </summary>
        public string SetName { get; set; }

        /// <summary>
        /// Set URI
        /// </summary>
        public string SetUri { get; set; }

        /// <summary>
        /// Set search URI
        /// </summary>
        public string SetSearchUri { get; set; }

        /// <summary>
        /// Scryfall set URI
        /// </summary>
        public string ScryfallSetUri { get; set; }

        /// <summary>
        /// Rulings URI
        /// </summary>
        public string RulingsUri { get; set; }

        /// <summary>
        /// Rulings 
        /// </summary>
        /// TODO: Convert to RulingsViewModel
        public Rulings Rulings { get; set; }

        /// <summary>
        /// Rarity
        /// </summary>
        public string Rarity { get; set; }

        /// <summary>
        /// Artist
        /// </summary>
        public string Artist { get; set; }

        /// <summary>
        /// Prices
        /// </summary>
        public Prices Prices { get; set; }

        /// <summary>
        /// Related URIs
        /// </summary>
        public RelatedUris RelatedUris { get; set; }

        /// <summary>
        /// PurchaseURIs
        /// </summary>
        public PurchaseUris PurchaseUris { get; set; }

        /// <summary>
        /// Loyalty
        /// </summary>
        public string Loyalty { get; set; }

        /// <summary>
        /// Flavor text
        /// </summary>
        public string FlavorText { get; set; }

        /// <summary>
        /// Flavor name
        /// </summary>
        public string FlavorName { get; set; }

        /// <summary>
        /// All parts if card is meld or similar
        /// </summary>
        public List<Part> AllParts { get; set; }

        /// <summary>
        /// True if card is multifaced
        /// </summary>
        public bool IsMultifaced { get; set; }

        /// <summary>
        /// True if card is reversible
        /// </summary>
        public bool IsReversible { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CardViewModel()
        {
        }

        /// <summary>
        /// Constructor from model
        /// </summary>
        /// <param name="model"></param>
        public CardViewModel(Card model)
        {
            Id = model.Id;
            OracleId = model.OracleId;
            OracleText = model.OracleText;
            Name = model.Name;
            Uri = model.Uri;
            ScryfallUri = model.ScryfallUri;
            Layout = model.Layout;
            ImageUris = model.ImageUris;
            ManaCost = model.ManaCost;
            ManaValue = model.ManaValue;
            TypeLine = model.TypeLine;
            Colors = model.Colors;
            ColorIndicator = model.ColorIndicator;
            ColorIdentity = model.ColorIdentity;
            ProducedMana = model.ProducedMana;
            Power = model.Power;
            Toughness = model.Toughness;
            Keywords = model.Keywords;
            Legalities = model.Legalities;
            SetId = model.SetId;
            SetCode = model.SetCode;
            SetName = model.SetName;
            SetUri = model.SetUri;
            SetSearchUri = model.SetSearchUri;
            ScryfallSetUri = model.ScryfallSetUri;
            RulingsUri = model.RulingsUri;
            Rarity = model.Rarity;
            Artist = model.Artist;
            Prices = model.Prices;
            RelatedUris = model.RelatedUris;
            PurchaseUris = model.PurchaseUris;
            Loyalty = model.Loyalty;
            FlavorText = model.FlavorText;
            FlavorName = model.FlavorName;
            AllParts = model.AllParts;
            IsMultifaced = model.IsMultifaced;
            IsReversible = model.IsReversible;
            CardFaces = model.CardFaces?.Select(f => new CardFaceViewModel(f)).ToList();
        }

        /// <summary>
        /// Get model
        /// </summary>
        /// <returns></returns>
        public Card GetModel()
        {
            return new()
            {
                Id = Id,
                OracleId = OracleId,
                OracleText = OracleText,
                Name = Name,
                Uri = Uri,
                ScryfallUri = ScryfallUri,
                Layout = Layout,
                ImageUris = ImageUris,
                ManaCost = ManaCost,
                ManaValue = ManaValue,
                TypeLine = TypeLine,
                Colors = Colors,
                ColorIndicator = ColorIndicator,
                ColorIdentity = ColorIdentity,
                ProducedMana = ProducedMana,
                Power = Power,
                Toughness = Toughness,
                Keywords = Keywords,
                Legalities = Legalities,
                SetId = SetId,
                SetCode = SetCode,
                SetName = SetName,
                SetUri = SetUri,
                SetSearchUri = SetSearchUri,
                ScryfallSetUri = ScryfallSetUri,
                RulingsUri = RulingsUri,
                Rarity = Rarity,
                Artist = Artist,
                Prices = Prices,
                RelatedUris = RelatedUris,
                PurchaseUris = PurchaseUris,
                Loyalty = Loyalty,
                FlavorText = FlavorText,
                FlavorName = FlavorName,
                AllParts = AllParts,
                CardFaces = CardFaces?.Select(f => f.GetModel()).ToList(),
            };
        }
    }
}