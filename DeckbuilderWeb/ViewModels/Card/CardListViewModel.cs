﻿using DeckbuilderWeb.Models.Cards;
using System.Text.Json.Serialization;

namespace DeckbuilderWeb.ViewModels.Cards
{
    /// <summary>
    /// Card list viewmodel
    /// </summary>
    public class CardListViewModel
    {
        /// <summary>
        /// Total cards : This field will contain the total number of cards found across all pages.
        /// </summary>
        public int TotalCards { get; set; }

        /// <summary>
        /// Has more : True if this List is paginated and there is a page beyond the current page.
        /// </summary>
        public bool HasMore { get; set; }

        /// <summary>
        /// Next page : If there is a page beyond the current page, this field will contain a full API URI to that page.
        /// You may submit a HTTP GET request to that URI to continue paginating forward on this List.
        /// </summary>
        public string NextPage { get; set; }

        /// <summary>
        /// Data
        /// </summary>
        public List<CardListItemViewModel> Cards { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CardListViewModel()
        {
        }

        /// <summary>
        /// Constructor from model
        /// </summary>
        /// <param name="model"></param>
        public CardListViewModel(CardList model)
        {
            TotalCards = model.TotalCards;
            HasMore = model.HasMore;
            NextPage = model.NextPage;
            Cards = model.Cards.Select(c => new CardListItemViewModel(c)).ToList();
        }
    }
}
