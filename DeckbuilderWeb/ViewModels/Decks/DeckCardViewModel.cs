﻿using DeckbuilderWeb.Models;
using DeckbuilderWeb.Models.Decks;

namespace DeckbuilderWeb.ViewModels.Decks
{
    /// <summary>
    /// Deck card viewmodel
    /// </summary>
    public class DeckCardViewModel : IViewModel<DeckCard>
    {
        /// <summary>
        /// Scryfall ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Quantity in deck
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Mana cost formated with brackets
        /// </summary>
        public string ManaCost { get; set; }

        /// <summary>
        /// Mana value
        /// </summary>
        public float ManaValue { get; set; }

        /// <summary>
        /// Colors
        /// </summary>
        public List<string> Colors { get; set; }

        /// <summary>
        /// Color identity
        /// </summary>
        public List<string> ColorIdentity { get; set; }

        /// <summary>
        /// Produced mana
        /// </summary>
        public List<string> ProducedMana { get; set; }

        /// <summary>
        /// Rarity
        /// </summary>
        public string Rarity { get; set; }

        /// <summary>
        /// Type line
        /// </summary>
        public string TypeLine { get; set; }

        /// <summary>
        /// Set code ( 3 letters)
        /// </summary>
        public string SetCode { get; set; }

        /// <summary>
        /// Set full name
        /// </summary>
        public string SetName { get; set; }

        /// <summary>
        /// Image of front face
        /// </summary>
        public string ImageFront { get; set; }

        /// <summary>
        /// Image of back face. Null on monofaced cards
        /// </summary>
        public string? ImageBack { get; set; }

        /// <summary>
        /// True if this card is a commander
        /// </summary>
        public bool IsCommander { get; set; }

        /// <summary>
        /// True if this card is a companion
        /// </summary>
        public bool IsCompanion { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public DeckCardViewModel()
        {
        }

        /// <summary>
        /// Constructor from model
        /// </summary>
        /// <param name="model"></param>
        public DeckCardViewModel(DeckCard model)
        {
            Id = model.Id;
            Quantity = model.Quantity;
            Name = model.Name;
            ManaCost = model.ManaCost;
            ManaValue = model.ManaValue;
            Colors = model.Colors;
            ColorIdentity = model.ColorIdentity;
            ProducedMana = model.ProducedMana;
            Rarity = model.Rarity;
            TypeLine = model.TypeLine;
            SetCode = model.SetCode;
            SetName = model.SetName;
            ImageFront = model.ImageFront;
            ImageBack = model.ImageBack;
            IsCommander = model.IsCommander;
            IsCompanion = model.IsCompanion;
        }

        /// <summary>
        /// Get model
        /// </summary>
        /// <returns></returns>
        public DeckCard GetModel()
        {
            return new()
            {
                Id = Id,
                Quantity = Quantity,
                Name = Name,
                ManaCost = ManaCost,
                ManaValue = ManaValue,
                Colors = Colors,
                ColorIdentity = ColorIdentity,
                ProducedMana = ProducedMana,
                Rarity = Rarity,
                TypeLine = TypeLine,
                SetCode = SetCode,
                SetName = SetName,
                ImageFront = ImageFront,
                ImageBack = ImageBack,
                IsCommander = IsCommander,
                IsCompanion = IsCompanion,
            };
        }
    }
}
