﻿using DeckbuilderWeb.Models.Decks;

namespace DeckbuilderWeb.ViewModels.Decks
{
    /// <summary>
    /// Deck list item viewmodel
    /// </summary>
    public class DeckListItemViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Cover image
        /// </summary>
        public string? CoverImage { get; set; }

        /// <summary>
        /// Format 
        /// </summary>
        public string Format { get; set; }

        /// <summary>
        /// True if deck shouldn't appear in public lists
        /// </summary>
        public bool IsPrivate { get; set; }

        /// <summary>
        /// Created at
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// UpdatedAt
        /// </summary>
        public DateTime UpdatedAt { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public DeckListItemViewModel()
        {
        }

        /// <summary>
        /// Constructor from model
        /// </summary>
        /// <param name="model"></param>
        public DeckListItemViewModel(Deck model)
        {
            Id = model.Id;
            Title = model.Title;
            CoverImage = model.CoverImage;
            Format = model.Format;
            IsPrivate = model.IsPrivate;
            CreatedAt = model.CreatedAt;
            UpdatedAt = model.UpdatedAt;
        }
    }
}