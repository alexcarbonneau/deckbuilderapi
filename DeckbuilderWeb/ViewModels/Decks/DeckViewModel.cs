﻿using System.ComponentModel;
using DeckbuilderWeb.Models;
using DeckbuilderWeb.Models.Decks;

namespace DeckbuilderWeb.ViewModels.Decks
{
    /// <summary>
    /// Deck details viewmodel
    /// </summary>
    public class DeckViewModel : IViewModel<Deck>
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// UserId 
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// Cover image
        /// </summary>
        [DisplayName("Cover image")]
        public string CoverImage { get; set; }

        /// <summary>
        /// Format 
        /// </summary>
        public string Format { get; set; }

        /// <summary>
        /// True if deck shouldn't appear in public lists
        /// </summary>
        [DisplayName("Private")]
        public bool IsPrivate { get; set; }

        /// <summary>
        /// True if deck doesn't need validation
        /// </summary>
        [DisplayName("Prototype")]
        public bool IsPrototype { get; set; }

        /// <summary>
        /// List of cards in the deck's main board
        /// </summary>
        public List<DeckCardViewModel> MainBoard { get; set; }

        /// <summary>
        /// List of cards in the deck's side board
        /// </summary>
        public List<DeckCardViewModel> SideBoard { get; set; }

        /// <summary>
        /// List of cards in the deck's maybe board
        /// </summary>
        public List<DeckCardViewModel> MaybeBoard { get; set; }

        /// <summary>
        /// Created at
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// UpdatedAt
        /// </summary>
        public DateTime UpdatedAt { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public DeckViewModel()
        {
        }

        /// <summary>
        /// Constructor from model
        /// </summary>
        /// <param name="model"></param>
        public DeckViewModel(Deck model)
        {
            Id = model.Id;
            UserId = model.UserId;
            Title = model.Title;
            Description = model.Description;
            CoverImage = model.CoverImage;
            Format = model.Format;
            IsPrivate = model.IsPrivate;
            IsPrototype = model.IsPrototype;
            MainBoard = model.MainBoard.Select(c => new DeckCardViewModel(c)).OrderBy(c => c.Name).ToList();
            SideBoard = model.SideBoard.Select(c => new DeckCardViewModel(c)).OrderBy(c => c.Name).ToList();
            MaybeBoard = model.MaybeBoard.Select(c => new DeckCardViewModel(c)).OrderBy(c => c.Name).ToList();
            CreatedAt = model.CreatedAt;
            UpdatedAt = model.UpdatedAt;
        }

        /// <summary>
        /// Get model
        /// </summary>
        /// <returns></returns>
        public Deck GetModel()
        {
            return new()
            {
                Id = Id,
                UserId = UserId,
                Title = Title,
                Description = Description,
                CoverImage = CoverImage,
                Format = Format,
                IsPrivate = IsPrivate,
                IsPrototype = IsPrototype,
                MainBoard = MainBoard.Select(c => c.GetModel()).ToList(),
                SideBoard = SideBoard.Select(c => c.GetModel()).ToList(),
                MaybeBoard = MaybeBoard.Select(c => c.GetModel()).ToList(),
                CreatedAt = CreatedAt,
                UpdatedAt = CreatedAt,
            };
        }
    }
}
