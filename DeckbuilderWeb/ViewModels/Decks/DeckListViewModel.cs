﻿using DeckbuilderWeb.Models;
using DeckbuilderWeb.Models.Decks;

namespace DeckbuilderWeb.ViewModels.Decks
{
    /// <summary>
    /// Deck list viewmodel
    /// </summary>
    public class DeckListViewModel
    {
        /// <summary>
        /// Total decks : This field will contain the total number of cards found across all pages.
        /// </summary>
        public int TotalDecks { get; set; }

        /// <summary>
        /// Data
        /// </summary>
        public List<DeckListItemViewModel> Decks { get; set; } = new List<DeckListItemViewModel>();

        /// <summary>
        /// Default constructor
        /// </summary>
        public DeckListViewModel()
        {
        }

        /// <summary>
        /// Constructor from model
        /// </summary>
        /// <param name="model"></param>
        public DeckListViewModel(DeckList model)
        {
            TotalDecks = model.TotalDecks;
            Decks = model.Decks.Select(d => new DeckListItemViewModel(d)).ToList();
        }
    }
}