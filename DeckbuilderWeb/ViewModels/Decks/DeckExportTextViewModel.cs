﻿using DeckbuilderWeb.Models.Decks;

namespace DeckbuilderWeb.ViewModels.Decks
{
    /// <summary>
    /// Deck export as text viewmodel
    /// </summary>
    public class DeckExportTextViewModel
    {
        /// <summary>
        /// Deck title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Main board
        /// </summary>
        public List<DeckCardViewModel> MainBoard { get; set; }

        /// <summary>
        /// Side board
        /// </summary>
        public List<DeckCardViewModel> SideBoard { get; set; }

        /// <summary>
        /// Maybe board
        /// </summary>
        public List<DeckCardViewModel> MaybeBoard { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public DeckExportTextViewModel()
        {
        }

        /// <summary>
        /// Constructor from model
        /// </summary>
        /// <param name="model"></param>
        public DeckExportTextViewModel(Deck model)
        {
            Title = model.Title;
            MainBoard = model.MainBoard.Select(c => new DeckCardViewModel(c)).OrderBy(c => c.Name).ToList();
            SideBoard = model.SideBoard.Select(c => new DeckCardViewModel(c)).OrderBy(c => c.Name).ToList();
            MaybeBoard = model.MaybeBoard.Select(c => new DeckCardViewModel(c)).OrderBy(c => c.Name).ToList();
        }
    }
}