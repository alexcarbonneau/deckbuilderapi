﻿using DeckbuilderWeb.Models.Cards;
using DeckbuilderWeb.Models.Queries;
using DeckbuilderWeb.Models.Search;
using System.Text.Json;

namespace DeckbuilderWeb.Services
{
    /// <summary>
    /// Scryfall implementation of card service
    /// </summary>
    public class ScryfallCardService : ICardService
    {
        private HttpClient _httpClient;

        /// <summary>
        /// Constructor
        /// </summary>
        public ScryfallCardService()
        {
            ConfigureHttpClient();
        }

        /// <summary>
        /// Configure HTTP client
        /// </summary>
        private void ConfigureHttpClient()
        {
            _httpClient = new HttpClient
            {
                // TODO : Move to configuration file
                BaseAddress = new Uri("https://api.scryfall.com/"),
            };
        }

        /// <summary>
        /// Get search async from query string
        /// </summary>
        /// <param name="query"></param>
        /// <returns>A card list</returns>
        public async Task<CardList> GetSearchAsync(string query)
        {
            var response = await _httpClient.GetAsync("cards/search?q=" + query);
            var content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<CardList>(content);
        }

        /// <summary>
        /// Get search async from advanced query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public Task<CardList> GetSearchAsync(CardSearchQuery query)
        {
            return GetSearchAsync(query.ToScryfallQuery());
        }

        /// <summary>
        /// Get async
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A single card</returns>
        public async Task<Card> GetAsync(string id)
        {
            var response = await _httpClient.GetAsync("/cards/" + id);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<Card>(content);
            }

            throw new HttpRequestException("No card found.");
        }

        /// <summary>
        /// Get by name asyunc
        /// </summary>
        /// <param name="cardName"></param>
        /// <returns></returns>
        /// <exception cref="HttpRequestException"></exception>
        public async Task<Card> GetByNameAsync(string cardName)
        {
            var response = await _httpClient.GetAsync("/cards/named/?exact=" + cardName);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<Card>(content);
            }

            throw new HttpRequestException("No card found");
        }

        /// <summary>
        /// Get rulings async
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Rulings> GetRulingsAsync(string id)
        {
            var response = await _httpClient.GetAsync("/cards/" + id + "/rulings");
            var content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<Rulings>(content);
        }

        /// <summary>
        /// Autocomplete by card name
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<CardAutoCompleteResponse> AutocompleteAsync(string query)
        {
            var response = await _httpClient.GetAsync("cards/autocomplete?q=" + query);
            var content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<CardAutoCompleteResponse>(content);
        }
    }
}
