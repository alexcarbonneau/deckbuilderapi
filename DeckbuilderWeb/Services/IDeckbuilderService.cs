﻿using DeckbuilderWeb.Models.Account;
using DeckbuilderWeb.Models.Cards;
using DeckbuilderWeb.Models.Decks;

namespace DeckbuilderWeb.Services
{
    /// <summary>
    /// Deckbuilder repository interface
    /// </summary>
    public interface IDeckbuilderService
    {
        /// <summary>
        /// Get user by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<User> GetUserAsync(string id);

        /// <summary>
        /// Check against DB to see if authenticated user exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<bool> IsUserRegisteredAsync(string id);

        /// <summary>
        /// Registers an new user to DB
        /// </summary>
        /// <returns></returns>
        public Task<bool> RegisterUserAsync(User user);

        /// <summary>
        /// Gets all decks
        /// </summary>
        /// <param name="includePrivate"></param>
        /// <returns></returns>
        public Task<DeckList> GetDecksAsync(bool includePrivate = false);

        /// <summary>
        /// Get all decks for a user
        /// </summary>
        /// <param name="includePrivate"></param>
        /// <param name="user_id"></param>
        /// <returns></returns>
        public Task<DeckList> GetDecksAsync(string user_id, bool includePrivate = false);

        /// <summary>
        /// Gets a deck by id
        /// </summary>
        /// <returns></returns>
        public Task<Deck> GetDeckAsync(int id);

        /// <summary>
        /// Create deck async
        /// </summary>
        /// <param name="deck"></param>
        /// <returns>ID of the created deck</returns>
        public Task<int> CreateDeckAsync(Deck deck);

        /// <summary>
        /// Edit deck async
        /// </summary>
        /// <param name="id"></param>
        /// <param name="deck"></param>
        /// <returns></returns>
        public Task<int> EditDeckAsync(int id, Deck deck);

        /// <summary>
        /// Add card to a deck
        /// </summary>
        /// <param name="card"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Deck> AddCardToDeckAsync(DeckCard card, int id);

        /// <summary>
        /// Delete deck async
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<bool> DeleteDeckAsync(int id);

        /// <summary>
        /// Increment quantity of a card by one in a deck
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Deck> AddOneToDeckAsync(string cardId, int id);

        /// <summary>
        /// Decrement quantity of a card by one in a deck
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Deck> RemoveOneFromDeckAsync(string cardId, int id);

        /// <summary>
        /// Set quantity in deck async
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="id"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        public Task<Deck> SetQuantityInDeckAsync(string cardId, int id, int quantity);

        /// <summary>
        /// Removes a card from a deck completely
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Deck> RemoveCardFromDeckAsync(string cardId, int id);

        /// <summary>
        /// Sets a card as a commander in a deck
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Deck> SetCardAsCommanderAsync(string cardId, int id);

        /// <summary>
        /// Sets a card as a the cover image of a deck
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Deck> ChangeCoverImageAsync(string cardId, int id);
    }
}
