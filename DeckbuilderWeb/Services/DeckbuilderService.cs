﻿using DeckbuilderWeb.Extensions;
using DeckbuilderWeb.Models.Account;
using DeckbuilderWeb.Models.Decks;
using System.Text;
using System.Text.Json;

namespace DeckbuilderWeb.Services
{
    /// <summary>
    /// DeckbuilderAPI implementation of deckbuilder service
    /// </summary>
    public class DeckbuilderService : IDeckbuilderService
    {
        private HttpClient _httpClient;

        /// <summary>
        /// Constructor
        /// </summary>
        public DeckbuilderService()
        {
            ConfigureHttpClient();
        }

        /// <summary>
        /// Configure HTTP client
        /// </summary>
        private void ConfigureHttpClient()
        {
            _httpClient = new HttpClient
            {
                // TODO : Move to configuration file
                BaseAddress = new Uri("https://localhost:7230/api/"),
            };
        }

        /// <summary>
        /// Get user by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<User> GetUserAsync(string id)
        {
            var response = await _httpClient.GetAsync("User/" + id);
            var content = await response.Content.ReadAsStringAsync();
            var user = JsonSerializer.Deserialize<User>(content);
            return user;
        }

        /// <summary>
        /// Checks if loggged in user is registered
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> IsUserRegisteredAsync(string id)
        {
            var response = await _httpClient.GetAsync("User/" + id);
            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Registers an empty user to DB
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<bool> RegisterUserAsync(User user)
        {
            var json = JsonSerializer.Serialize(user);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync("User", content);
            return response.IsSuccessStatusCode;
        }


        /// <summary>
        /// Get all decks (only public decks by default)
        /// </summary>
        /// <param name="includePrivate"></param>
        /// <returns></returns>
        public async Task<DeckList> GetDecksAsync(bool includePrivate = false)
        {
            var response = await _httpClient.GetAsync("Deck/?includePrivate=" + includePrivate);
            var content = await response.Content.ReadAsStringAsync();
            var decks = JsonSerializer.Deserialize<DeckList>(content);
            return decks;
        }

        /// <summary>
        /// Get all decks for a user (only public ones by default)
        /// </summary>
        /// <param name="includePrivate"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<DeckList> GetDecksAsync(string userId, bool includePrivate = false)
        {
            var response = await _httpClient.GetAsync("Deck/?userId=" + userId + "&includePrivate=" + includePrivate);
            var content = await response.Content.ReadAsStringAsync();
            var decks = JsonSerializer.Deserialize<DeckList>(content);
            return decks;
        }

        /// <summary>
        /// Gets a deck by id
        /// </summary>
        /// <returns></returns>
        public async Task<Deck> GetDeckAsync(int id)
        {
            var response = await _httpClient.GetAsync("Deck/" + id);
            var content = await response.Content.ReadAsStringAsync();
            var deck = JsonSerializer.Deserialize<Deck>(content);
            return deck;
        }

        /// <summary>
        /// Create deck async
        /// </summary>
        /// <param name="deck"></param>
        /// <returns></returns>
        public async Task<int> CreateDeckAsync(Deck deck)
        {
            deck.Clean();

            var jsonDeck = JsonSerializer.Serialize(deck);

            var response = await _httpClient.PostAsJsonAsync("Deck", jsonDeck);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var newDeck = JsonSerializer.Deserialize<Deck>(content);

                if (newDeck is null) throw new HttpRequestException();

                return newDeck.Id;
            }

            throw new HttpRequestException();
        }

        /// <summary>
        /// Edit deck async
        /// </summary>
        /// <param name="id"></param>
        /// <param name="deck"></param>
        /// <returns></returns>
        public async Task<int> EditDeckAsync(int id, Deck deck)
        {
            if (id != deck.Id) { throw new BadHttpRequestException("The request isn't formatted propetly", StatusCodes.Status418ImATeapot); }
            deck.Clean();

            var jsonDeck = JsonSerializer.Serialize(deck);

            var response = await _httpClient.PutAsJsonAsync("Deck/" + id, jsonDeck);

            return (response.IsSuccessStatusCode) ? id : -1;
        }

        /// <summary>
        /// Delete deck async
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteDeckAsync(int id)
        {
            var response = await _httpClient.DeleteAsync("Deck/" + id);

            return response.IsSuccessStatusCode;

            throw new HttpRequestException();
        }
        /// <summary>
        /// Add card to deck async
        /// </summary>
        /// <param name="card"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Deck> AddCardToDeckAsync(DeckCard card, int id)
        {
            var response = await _httpClient.GetAsync("Deck/" + id);
            var content = await response.Content.ReadAsStringAsync();
            var deck = JsonSerializer.Deserialize<Deck>(content);

            if (deck is null) return null;

            if (deck.MainBoard.Any(c => c.Name.Equals(card.Name)))
            {
                deck.MainBoard.First(c => c.Id == card.Id).Quantity += 1;
            }
            else
            {
                deck.MainBoard.Add(card);
            }

            deck.Clean();

            var jsonDeck = JsonSerializer.Serialize(deck);

            response = await _httpClient.PutAsJsonAsync("Deck/" + id, jsonDeck);

            if (response.IsSuccessStatusCode)
            {
                return deck;
            }
            else
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Increment quantity of a card by one in a deck
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Deck> AddOneToDeckAsync(string cardId, int id)
        {
            var response = await _httpClient.GetAsync("Deck/" + id);
            var content = await response.Content.ReadAsStringAsync();
            var deck = JsonSerializer.Deserialize<Deck>(content);

            if (deck is null) return null;

            if (deck.MainBoard.Any(c => c.Id.Equals(cardId)))
            {
                deck.MainBoard.First(c => c.Id.Equals(cardId)).Quantity += 1;
            }

            deck.Clean();

            var jsonDeck = JsonSerializer.Serialize(deck);

            response = await _httpClient.PutAsJsonAsync("Deck/" + id, jsonDeck);

            if (response.IsSuccessStatusCode)
            {
                return deck;
            }
            else
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Decrement quantity of a card by one in a deck
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Deck> RemoveOneFromDeckAsync(string cardId, int id)
        {
            var response = await _httpClient.GetAsync("Deck/" + id);
            var content = await response.Content.ReadAsStringAsync();
            var deck = JsonSerializer.Deserialize<Deck>(content);

            if (deck is null) return null;

            if (deck.MainBoard.Any(c => c.Id.Equals(cardId)))
            {
                deck.MainBoard.First(c => c.Id.Equals(cardId)).Quantity -= 1;
            }

            deck.Clean();

            var jsonDeck = JsonSerializer.Serialize(deck);

            response = await _httpClient.PutAsJsonAsync("Deck/" + id, jsonDeck);

            if (response.IsSuccessStatusCode)
            {
                return deck;
            }
            else
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Removes a card from a deck completely
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Deck> RemoveCardFromDeckAsync(string cardId, int id)
        {
            var response = await _httpClient.GetAsync("Deck/" + id);
            var content = await response.Content.ReadAsStringAsync();
            var deck = JsonSerializer.Deserialize<Deck>(content);

            if (deck is null) return null;

            if (deck.MainBoard.Any(c => c.Id.Equals(cardId)))
            {
                deck.MainBoard.First(c => c.Id.Equals(cardId)).Quantity = 0;
            }

            deck.Clean();

            var jsonDeck = JsonSerializer.Serialize(deck);

            response = await _httpClient.PutAsJsonAsync("Deck/" + id, jsonDeck);

            if (response.IsSuccessStatusCode)
            {
                return deck;
            }
            else
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Set quantity in deck async
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="id"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        public async Task<Deck> SetQuantityInDeckAsync(string cardId, int id, int quantity)
        {
            var response = await _httpClient.GetAsync("Deck/" + id);
            var content = await response.Content.ReadAsStringAsync();
            var deck = JsonSerializer.Deserialize<Deck>(content);

            if (deck is null) return null;

            if (deck.MainBoard.Any(c => c.Id.Equals(cardId)))
            {
                deck.MainBoard.First(c => c.Id.Equals(cardId)).Quantity = quantity;
            }

            deck.Clean();

            var jsonDeck = JsonSerializer.Serialize(deck);

            response = await _httpClient.PutAsJsonAsync("Deck/" + id, jsonDeck);

            if (response.IsSuccessStatusCode)
            {
                return deck;
            }
            else
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Sets a card as a commander in a deck
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Deck> SetCardAsCommanderAsync(string cardId, int id)
        {
            var response = await _httpClient.GetAsync("Deck/" + id);
            var content = await response.Content.ReadAsStringAsync();
            var deck = JsonSerializer.Deserialize<Deck>(content);

            if (deck is null) return null;

            if (deck.MainBoard.Any(c => c.Id.Equals(cardId)))
            {
                if (deck.MainBoard.Any(c => c.IsCommander))
                {
                    deck.MainBoard.First(c => c.IsCommander).IsCommander = false;
                }

                deck.MainBoard.First(c => c.Id.Equals(cardId)).IsCommander = true;
            }

            deck.Clean();

            var jsonDeck = JsonSerializer.Serialize(deck);

            response = await _httpClient.PutAsJsonAsync("Deck/" + id, jsonDeck);

            if (response.IsSuccessStatusCode)
            {
                return deck;
            }
            else
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Change deck cover image
        /// </summary>
        /// <param name="imageUri"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Deck> ChangeCoverImageAsync(string imageUri, int id)
        {
            var response = await _httpClient.GetAsync("Deck/" + id);
            var content = await response.Content.ReadAsStringAsync();
            var deck = JsonSerializer.Deserialize<Deck>(content);

            if (deck is null) return null;

            deck.CoverImage = imageUri;

            var jsonDeck = JsonSerializer.Serialize(deck);

            response = await _httpClient.PutAsJsonAsync("Deck/" + id, jsonDeck);

            if (response.IsSuccessStatusCode)
            {
                return deck;
            }
            else
            {
                throw new Exception();
            }
        }

    }
}
