﻿using DeckbuilderWeb.Models.Cards;
using DeckbuilderWeb.Models.Queries;
using DeckbuilderWeb.Models.Search;

namespace DeckbuilderWeb.Services
{
    /// <summary>
    /// MTG Card repository interface
    /// </summary>
    public interface ICardService
    {
        /// <summary>
        /// Get search async
        /// </summary>
        /// <returns></returns>
        public Task<CardList> GetSearchAsync(string query);

        /// <summary>
        /// Get search async
        /// </summary>
        /// <returns></returns>
        public Task<CardList> GetSearchAsync(CardSearchQuery query);

        /// <summary>
        /// Get async
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Card> GetAsync(string id);

        /// <summary>
        /// Get by name async
        /// </summary>
        /// <param name="name"></param>
        public Task<Card> GetByNameAsync(string name);

        /// <summary>
        /// Get rulings async
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Rulings> GetRulingsAsync(string id);

        /// <summary>
        /// Autocomplete by card name.
        /// </summary>
        /// <param name="query"></param>
        /// <returns>A list of possible results for the full or partial given name</returns>
        public Task<CardAutoCompleteResponse> AutocompleteAsync(string query);
    }
}
