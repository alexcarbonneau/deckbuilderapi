﻿using System.Text.Json.Serialization;

namespace DeckbuilderAPI.Models.Deck
{
    /// <summary>
    /// Deck list
    /// </summary>
    public class DeckList
    {

        /// <summary>
        /// Total cards
        /// </summary>
        [JsonPropertyName("total_decks")]
        public int TotalDecks { get => Decks.Count; }

        /// <summary>
        /// Decks
        /// </summary>
        [JsonPropertyName("decks")]
        public List<Deck> Decks { get; set; }
    }
}
