﻿using DeckbuilderAPI.Models.Account;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace DeckbuilderAPI.Models.Deck
{
    /// <summary>
    /// Basic model for a deck
    /// </summary>
    public class Deck : BaseEntity
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [JsonPropertyName("id")]
        public int Id { get; set; }

        /// <summary>
        /// UserId 
        /// </summary>
        [JsonPropertyName("user_id")]
        public string UserId { get; set; }

        /// <summary>
        /// User
        /// </summary>
        [JsonIgnore]
        public User? User { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        [MaxLength(100)]
        [JsonPropertyName("title")]
        public string Title { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        [MaxLength(1000)]
        [JsonPropertyName("description")]
        public string? Description { get; set; }

        /// <summary>
        /// Cover image
        /// </summary>
        [JsonPropertyName("cover_image")]
        public string CoverImage { get; set; }

        /// <summary>
        /// Format 
        /// </summary>
        [MaxLength(100)]
        [JsonPropertyName("format")]
        public string Format { get; set; }

        /// <summary>
        /// True if deck shouldn't appear in public lists
        /// </summary>
        [JsonPropertyName("is_private")]
        public bool IsPrivate { get; set; }

        /// <summary>
        /// True if deck doesn't need validation
        /// </summary>
        [JsonPropertyName("is_prototype")]
        public bool IsPrototype { get; set; }

        /// <summary>
        /// List of cards in the deck's main board
        /// </summary>
        [NotMapped]
        [JsonPropertyName("main_board")]
        public List<DeckCard> MainBoard
        {
            get => JsonSerializer.Deserialize<List<DeckCard>>(JsonMainBoard);
            set => JsonMainBoard = JsonSerializer.Serialize(value);
        }

        /// <summary>
        /// List of cards in the deck's side board
        /// </summary>
        [NotMapped]
        [JsonPropertyName("side_board")]
        public List<DeckCard> SideBoard
        {
            get => JsonSerializer.Deserialize<List<DeckCard>>(JsonSideBoard);
            set => JsonSideBoard = JsonSerializer.Serialize(value);
        }

        /// <summary>
        /// List of cards in the deck's maybe board
        /// </summary>
        [NotMapped]
        [JsonPropertyName("maybe_board")]
        public List<DeckCard> MaybeBoard
        {
            get => JsonSerializer.Deserialize<List<DeckCard>>(JsonMaybeBoard);
            set => JsonMaybeBoard = JsonSerializer.Serialize(value);
        }

        /// <summary>
        /// JSON string representation of Mainboard saved to the database
        /// </summary>
        [JsonIgnore]
        public string JsonMainBoard { get; set; }

        /// <summary>
        /// JSON string representation of Sideboard saved to the database
        /// </summary>
        [JsonIgnore]
        public string JsonSideBoard { get; set; }

        /// <summary>
        /// JSON string representation of Maybeboard saved to the database
        /// </summary>
        [JsonIgnore]
        public string JsonMaybeBoard { get; set; }
    }
}
