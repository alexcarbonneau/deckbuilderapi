﻿using System.Text.Json.Serialization;

namespace DeckbuilderAPI.Models.Deck
{
    /// <summary>
    /// Deck card model.
    /// This is an internal model that will be saved as a Json string in a Deck row.
    /// </summary>
    [Serializable]
    public class DeckCard
    {
        /// <summary>
        /// Scryfall ID
        /// </summary>
        [JsonPropertyName("id")]
        public string Id { get; set; }

        /// <summary>
        /// Quantity in deck
        /// </summary>
        [JsonPropertyName("quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonPropertyName("name")]
        public string Name { get; set; }

        /// <summary>
        /// Mana cost formated with brackets. Ex : {2}{U}{B}
        /// </summary>
        [JsonPropertyName("mana_cost")]
        public string ManaCost { get; set; }

        /// <summary>
        /// Mana value a.k.a Converted mana cost
        /// </summary>
        [JsonPropertyName("cmc")]
        public float ManaValue { get; set; }

        /// <summary>
        /// Colors
        /// </summary>
        [JsonPropertyName("colors")]
        public List<string> Colors { get; set; }

        /// <summary>
        /// Colors
        /// </summary>
        [JsonPropertyName("color_identity")]
        public List<string> ColorIdentity { get; set; }

        /// <summary>
        /// Produced mana
        /// </summary>
        [JsonPropertyName("produced_mana")]
        public List<string> ProducedMana { get; set; }

        /// <summary>
        /// Rarity
        /// </summary>
        [JsonPropertyName("rarity")]
        public string Rarity { get; set; }

        /// <summary>
        /// Type line
        /// </summary>
        [JsonPropertyName("type_line")]
        public string TypeLine { get; set; }

        /// <summary>
        /// Set code ( 3 letters)
        /// </summary>
        [JsonPropertyName("set")]
        public string SetCode { get; set; }

        /// <summary>
        /// Set full name
        /// </summary>
        [JsonPropertyName("set_name")]
        public string SetName { get; set; }

        /// <summary>
        /// Image of front face
        /// </summary>run
        [JsonPropertyName("image_front")]
        public string ImageFront { get; set; }

        /// <summary>
        /// Image of back face. Null on monofaced cards
        /// </summary>
        [JsonPropertyName("image_back")]
        public string? ImageBack { get; set; }

        /// <summary>
        /// True if this card is a commander
        /// </summary>
        [JsonPropertyName("is_commander")]
        public bool IsCommander { get; set; }

        /// <summary>
        /// True if this card is a companion
        /// </summary>
        [JsonPropertyName("is_companion")]
        public bool IsCompanion { get; set; }

        /// <summary>
        /// Equals method override
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object? obj)
        {
            if (obj is null) return false;

            var card = (DeckCard) obj;

            if (card is null) return false;

            if (!card.Name.Equals(Name)) return false;

            return true;
        }

        /// <summary>
        /// Get hash code override
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
