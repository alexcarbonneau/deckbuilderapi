﻿using System.Text.Json.Serialization;

namespace DeckbuilderAPI.Models
{
    /// <summary>
    /// Base class for all DB entities
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// Created at
        /// </summary>
        [JsonPropertyName("created_at")]
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// UpdatedAt
        /// </summary>
        [JsonPropertyName("updated_at")]
        public DateTime UpdatedAt { get; set;}
    }
}
