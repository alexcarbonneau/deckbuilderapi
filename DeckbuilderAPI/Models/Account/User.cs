﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DeckbuilderAPI.Models.Account
{
    /// <summary>
    /// User
    /// </summary>
    public class User : BaseEntity
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [JsonPropertyName("id")]
        public string Id { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [JsonPropertyName("email")]
        public string Email { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonPropertyName("name")]
        public string? Name { get; set; }

        /// <summary>
        /// Bio
        /// </summary>
        [JsonPropertyName("bio")]
        public string? Bio { get; set; }

        /// <summary>
        /// Decks
        /// </summary>
        [JsonIgnore]
        public ICollection<Deck.Deck>? Decks { get; set; }
    }
}
