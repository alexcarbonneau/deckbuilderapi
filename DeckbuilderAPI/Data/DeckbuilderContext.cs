﻿using DeckbuilderAPI.Models;
using DeckbuilderAPI.Models.Account;
using DeckbuilderAPI.Models.Deck;
using Microsoft.EntityFrameworkCore;

namespace DeckbuilderAPI.Data
{
    /// <summary>
    /// Deckbuilder DB context
    /// </summary>
    public class DeckbuilderContext : DbContext
    {
        /// <summary>
        /// Configuration [DI]
        /// </summary>
        protected readonly IConfiguration Configuration;

        /// <summary>
        ///  Default constructoronf
        /// </summary>
        /// <param name="configuration"></param>
        public DeckbuilderContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// OnConfiguring
        /// </summary>
        /// <param name="options"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(Configuration.GetConnectionString("SqlExpress"));
        }

        /// <summary>
        /// OnModelCreating
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
              .HasMany(user => user.Decks)
              .WithOne(deck => deck.User);
        }

        /// <summary>
        /// SaveChangesAsync with timestamps
        /// </summary>
        /// <returns></returns>
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is BaseEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                ((BaseEntity)entityEntry.Entity).UpdatedAt = DateTime.Now;

                if (entityEntry.State == EntityState.Added)
                {
                    ((BaseEntity)entityEntry.Entity).CreatedAt = DateTime.Now;
                }
            }

            return await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// User
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// Decks
        /// </summary>
        public DbSet<Deck> Decks { get; set; }

    }
}
