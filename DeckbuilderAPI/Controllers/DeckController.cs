﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DeckbuilderAPI.Data;
using DeckbuilderAPI.Models.Deck;
using Microsoft.AspNetCore.Authorization;
using System.Text.Json;

namespace DeckbuilderAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeckController : ControllerBase
    {
        private readonly DeckbuilderContext _context;

        public DeckController(DeckbuilderContext context)
        {
            _context = context;
        }

        // GET: api/Deck
        [HttpGet]
        public async Task<ActionResult<DeckList>> GetDecks(string userId, bool includePrivate)
        {
            List<Deck> decks;

            if (!includePrivate && !string.IsNullOrEmpty(userId))
            {
                decks = await _context.Decks
                   .Where(d => d.IsPrivate == false && d.UserId == userId)
                   .ToListAsync();
            }
            else if (includePrivate && !string.IsNullOrEmpty(userId))
            {
                decks =  await _context.Decks
                   .Where(d => d.UserId == userId)
                   .ToListAsync();

            }
            else if (!includePrivate && string.IsNullOrEmpty(userId))
            {
                decks = await _context.Decks
                    .Where(d => d.IsPrivate == false)
                    .ToListAsync();
            }
            else
            {
                decks = await _context.Decks
                    .ToListAsync();
            }
            
            return new DeckList() { Decks = decks };
        }

        // GET: api/Deck/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Deck>> GetDeck(int id)
        {
            var deck = await _context.Decks.FindAsync(id);

            if (deck == null)
            {
                return NotFound();
            }

            return deck;
        }

        // PUT: api/Deck/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDeck(int id, [FromBody] string deck)
        {
            Deck toUpdate = JsonSerializer.Deserialize<Deck>(deck);
            if (id != toUpdate.Id)
            {
                return BadRequest();
            }

            _context.Entry(toUpdate).State = EntityState.Modified;
            _context.Update(toUpdate);

            try
            {
                await _context.SaveChangesAsync();
            }

            catch (DbUpdateConcurrencyException)
            {
                if (!DeckModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Deck
        [HttpPost]
        public async Task<ActionResult<Deck>> PostDeck([FromBody] string deck)
        {
            Deck toCreate = JsonSerializer.Deserialize<Deck>(deck);

            _context.Decks.Add(toCreate);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDeck", new { id = toCreate.Id }, toCreate);
        }

        // DELETE: api/Deck/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDeck(int id)
        {
            var deck = await _context.Decks.FindAsync(id);
            if (deck == null)
            {
                return NotFound();
            }

            _context.Decks.Remove(deck);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool DeckModelExists(int id)
        {
            return _context.Decks.Any(e => e.Id == id);
        }
    }
}
